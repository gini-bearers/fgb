import os
import numpy as np
import pandas as pd
import time
from IPython.display import display, Markdown
import gzip
import pickle

class DataStorage:
    df = pd.DataFrame()

    def __init__(self, feature_groups={}, save_fields=[], categorical_features=[], target='', df=False, queries={}):
        '''
        initialized function

        Parameters :
        -------------
        df : dataframe with data
        queries : str, query to split df
        feature_groups : dict, groups of features, ex: {'bki': ['f_bki_1', 'f_bki_2']}
        '''
        
        try:
            self.df = df
            self.categorical_features = categorical_features
            self.queries = queries
            self.feature_groups = feature_groups
            self.save_fields = save_fields
            self.target = target
            
            for i in self.queries:
                setattr(self, str(i), self.queries[i])
        except FileExistsError:
            print('error load')
          
    def __getattribute__(self, key):
        if key in list(object.__getattribute__(self, 'queries')):
            return self.get_subsample(key)
        else:
            return object.__getattribute__(self, key)

    def get_subsample(self, name_queries):
        '''
        Get df by name

        Parameters :
        -------------
        name_queries : name
            Name query 

        '''
        if name_queries in self.queries:
            return self.df.query(self.queries[name_queries])
        else:
            print('Not find this set')
    
    def get_feature_subset(self, groups):
        '''
        Get df by feature groups

        Parameters :
        -------------
        groups : list or str
            list of group names
        '''
        if type(groups) == str:
            groups = [groups]
        feats = []
        for gr in groups:
            if gr in self.feature_groups:
                feats += self.feature_groups[gr]
            else:
                print(f'{gr} not found in groups')
        return self.df[feats]

    def get_feature_groups(self, groups):
        '''
        Get groups

        Parameters :
        -------------
        groups : list
            list of group names
        '''
        feats = {}
        for gr in groups:
            if gr in self.feature_groups:
                feats[gr] = self.feature_groups[gr]
            else:
                print(f'{gr} not found in groups')
        return feats
        
            
    def save_to_file(self, save_name):
        save_dump = {}
        save_dump['df'] = self.df
        save_dump['queries'] = self.queries
        save_dump['feature_groups'] = self.feature_groups
        
        with gzip.open(save_name + '.gz', 'wb') as f:
            pickle.dump(save_dump, f)
        
    def load_from_file(self, save_name):        
        with gzip.open(save_name + '.gz', 'rb') as f:            
            save_dump = pickle.load(f)
        
        self.df = save_dump['df']
        self.queries = save_dump['queries']
        self.feature_groups = save_dump['feature_groups']
        
        for i in self.queries:
            setattr(self, str(i), self.queries[i])

        