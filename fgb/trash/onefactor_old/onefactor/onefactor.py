from sklearn.metrics import roc_auc_score
import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn.metrics import mutual_info_score
from catboost import CatBoostClassifier
import math
from IPython.display import clear_output
from optbinning import OptimalBinning



class OneFactorAnalysis:
    def __init__(self
                 , queries  # все queries
                 , pairs  # для метрик без таргета
                 , query_target_dict  # для метрик с таргетом
                 , checks  # список функций расчета метрик
                 , ignore_features
                 , save_features
                 , verbose_clear=True
                 ):
        self.queries = queries
        self.pairs = pairs
        self.query_target_dict = query_target_dict
        self.checks = checks
        self.ignore_features = ignore_features
        self.save_features = save_features
        self.verbose_clear = verbose_clear

    def calculate(self, df, verbose=True):
        self.research = pd.DataFrame(list(df), columns=['name'])
        for check in self.checks:
            if verbose:
                print(check)
            self.research = self.research.merge(self.checks[check]({
                'df': df.copy(),
                'queries': self.queries.copy(),
                'pairs': self.pairs.copy(),
                'query_target_dict': self.query_target_dict.copy(),
                'checks': self.checks.copy(),
                'ignore_features': self.ignore_features.copy(),
                'verbose_clear': self.verbose_clear
            }), on='name')

        return self.research

    def calculate_consistently(self, df, checks_continue):
        self.research = pd.DataFrame(list(df), columns=['name'])

        df_temp = df.copy()
        for check in checks_continue:
            self.research = self.research.merge(check['check']({
                'df': df_temp,
                'queries': self.queries.copy(),
                'pairs': self.pairs.copy(),
                'query_target_dict': self.query_target_dict.copy(),
                'checks': self.checks.copy(),
                'ignore_features': self.ignore_features.copy()
            }), on='name')

            conclusion, feature_next_step = check['condition'](research_previous_step=self.research,
                                                               field_for_attraction=check['field'])

            save_features = list(set(self.save_features) - set(feature_next_step))

            df_temp = df_temp[feature_next_step + save_features]
            self.research = self.research.merge(conclusion, on='name')

        return self.research


class Checks:
    '''
        Расчет общих характеристик. Нужен только сам датасет
    '''

    @staticmethod
    def calc_feature_stats_summary(args):
        res = pd.DataFrame(list(args['df'].drop), columns=['name'])
        row = []
        for i in tqdm(args['df'].columns):
            one_row = Checks.calc_feature_stats(args['df'][i], i)
            row.append(one_row)

        return pd.DataFrame(row)

    '''    
        Расчет всех характеристик для одной переменной, столбец и название переменной
    '''

    @staticmethod
    def calc_feature_stats(f, name):
        res = {}
        res['name'] = name

        res['n_unique'] = f.nunique(dropna=False)
        res['NaN%'] = f.isna().mean()
        if res['n_unique'] > 1:
            res['max_concentration'] = f.value_counts().head(1).values[0] / len(f)
            res['max_concentration_value'] = f.value_counts().head(1).index[0]
        else:
            res['max_concentration'] = np.nan
            res['max_concentration_value'] = np.nan

        try:
            f = f.astype(float)
        except:
            return res
        res['max'] = f.max()
        res['min'] = f.min()
        res['std'] = f.std()
        res['mean'] = f.mean()
        res['median'] = f.median()

        return res

    @staticmethod
    def calc_psi(args, verbose=False):
        queries = args['queries']
        pairs = args['pairs']
        df = args['df']
        ignore_features = args['ignore_features']

        #         res = pd.DataFrame(list(args['df']), columns=['name'])
        row = []
        for pair in pairs:
            from_p = list(pair)[0]
            to_p = pair[from_p]
            expected = df.query(queries[to_p])
            actual = df.query(queries[from_p])
            for i in tqdm(df.drop(columns=ignore_features).columns, disable=not verbose):

                if i in ignore_features:
                    continue

                try:
                    exp = expected[i]
                    act = actual[i]
                    exp = exp.astype(float).fillna(exp.median())
                    act = act.astype(float).fillna(act.median())
                    psi = Checks.calculate_psi(exp, act, buckettype='bins', buckets=10)
                    row.append({'name': i, 'psi_' + str(from_p) + '_' + str(to_p): psi})
                except:
                    row.append({'name': i, 'psi_' + str(from_p) + '_' + str(to_p): np.nan})

        return pd.DataFrame(row)

    @staticmethod
    def calculate_psi(expected, actual, buckettype='bins', buckets=10, axis=0):
        '''Calculate the PSI (population stability index) across all variables
        Args:
           expected: numpy matrix of original values
           actual: numpy matrix of new values, same size as expected
           buckettype: type of strategy for creating buckets, bins splits into even splits, quantiles splits into quantile buckets
           buckets: number of quantiles to use in bucketing variables
           axis: axis by which variables are defined, 0 for vertical, 1 for horizontal
        Returns:
           psi_values: ndarray of psi values for each variable
        Author:
           Matthew Burke
           github.com/mwburke
           worksofchart.com
        '''

        def psi(expected_array, actual_array, buckets):
            '''Calculate the PSI for a single variable
            Args:
               expected_array: numpy array of original values
               actual_array: numpy array of new values, same size as expected
               buckets: number of percentile ranges to bucket the values into
            Returns:
               psi_value: calculated PSI value
            '''

            def scale_range(input, min, max):
                input += -(np.min(input))
                input /= np.max(input) / (max - min)
                input += min
                return input

            breakpoints = np.arange(0, buckets + 1) / (buckets) * 100

            if buckettype == 'bins':
                breakpoints = scale_range(breakpoints, np.min(expected_array), np.max(expected_array))
            elif buckettype == 'quantiles':
                breakpoints = np.stack([np.percentile(expected_array, b) for b in breakpoints])

            expected_percents = np.histogram(expected_array, breakpoints)[0] / len(expected_array)
            actual_percents = np.histogram(actual_array, breakpoints)[0] / len(actual_array)

            def sub_psi(e_perc, a_perc):
                '''Calculate the actual PSI value from comparing the values.
                   Update the actual value to a very small number if equal to zero
                '''
                if a_perc == 0:
                    a_perc = 0.0001
                if e_perc == 0:
                    e_perc = 0.0001

                value = (e_perc - a_perc) * np.log(e_perc / a_perc)
                return (value)

            psi_value = np.sum(
                sub_psi(expected_percents[i], actual_percents[i]) for i in range(0, len(expected_percents)))

            return (psi_value)

        if len(expected.shape) == 1:
            psi_values = np.empty(len(expected.shape))
        else:
            psi_values = np.empty(expected.shape[axis])

        for i in range(0, len(psi_values)):
            if len(psi_values) == 1:
                psi_values = psi(expected, actual, buckets)
            elif axis == 0:
                psi_values[i] = psi(expected[:, i], actual[:, i], buckets)
            elif axis == 1:
                psi_values[i] = psi(expected[i, :], actual[i, :], buckets)

        return (psi_values)

    @staticmethod
    def calc_gini(args):
        queries = args['queries']
        pairs = args['pairs']
        df = args['df']
        query_target_dict = args['query_target_dict']
        ignore_features = args['ignore_features']
        verbose_clear = args['verbose_clear']
        res = pd.DataFrame(list(df.drop(columns=ignore_features)), columns=['name'])
        for q in query_target_dict:
            df_query = df.query(queries[q])

            for t in query_target_dict[q]:
                if verbose_clear:
                    clear_output()
                print('query', q, 'target', t)
                for i in tqdm(df.drop(columns=ignore_features).columns): #, disable=not verbose):

                    if i in ignore_features:
                        continue

                    try:
                        d_xy = df_query[(df_query[i].notna()) & (df_query[t].notna())]
                        x = d_xy[i]
                        y = d_xy[t]
                        res.loc[res['name'] == i, 'gini_' + str(q) + '_' + str(t)] = abs(2 * roc_auc_score(y, x) - 1)
                    except Exception as inst:
                        #                         print(q, t, i, inst)
                        res.loc[res['name'] == i, 'gini_' + str(q) + '_' + str(t)] = np.nan

        return res

    @staticmethod
    def calc_ig(args, verbose=False):
        queries = args['queries']
        pairs = args['pairs']
        df = args['df']
        query_target_dict = args['query_target_dict']
        ignore_features = args['ignore_features']
        verbose_clear = args['verbose_clear']

        res = pd.DataFrame(list(df), columns=['name'])
        for q in query_target_dict:
            q_df = df.query(queries[q])
            for t in query_target_dict[q]:
                if verbose_clear:
                    clear_output()
                if verbose:
                    print('query', q, 'target', t)
                for i in tqdm(df.drop(columns=ignore_features).columns, disable= not verbose):

                    if i in ignore_features:
                        continue

                    try:
                        #                         x_score = df.query(queries[q])[i]
                        #                         y_true = df.query(queries[q])[t]

                        mi = Checks.__calc_information_gain(q_df, i, t)

                        res.loc[res['name'] == i, 'mi_' + str(q) + '_' + str(t)] = mi
                    except Exception as inst:
                        print(q, t, i, inst)
                        res.loc[res['name'] == i, 'mi_' + str(q) + '_' + str(t)] = np.nan

        return res

    @staticmethod
    def __calc_entropy(column):
        """
        Calculate entropy given a pandas series, list, or numpy array.
        """
        # Compute the counts of each unique value in the column
        counts = np.bincount(column)
        # Divide by the total column length to get a probability
        probabilities = counts / len(column)

        # Initialize the entropy to 0
        entropy = 0
        # Loop through the probabilities, and add each one to the total entropy
        for prob in probabilities:
            if prob > 0:
                # use log from math and set base to 2
                entropy += prob * math.log(prob, 2)

        return -entropy

    @staticmethod
    def __calc_information_gain(data, split_name, target_name):
        """
        Calculate information gain given a data set, column to split on, and target
        """
        # Calculate the original entropy
        original_entropy = Checks.__calc_entropy(data[target_name])

        # Find the unique values in the column
        values = data[split_name].unique()

        # Make two subsets of the data, based on the unique values
        left_split = data[data[split_name] == values[0]]
        right_split = data[data[split_name] == values[1]]

        # Loop through the splits and calculate the subset entropies
        to_subtract = 0
        for subset in [left_split, right_split]:
            prob = (subset.shape[0] / data.shape[0])
            to_subtract += prob * Checks.__calc_entropy(subset[target_name])

        # Return information gain
        return original_entropy - to_subtract

    @staticmethod
    def calc_iv(args):
        queries = args['queries']
        df = args['df']
        query_target_dict = args['query_target_dict']
        ignore_features = args['ignore_features']
        verbose_clear = args['verbose_clear']
        binning_params = {'solver': 'cp', 'monotonic_trend': 'auto_asc_desc'}
        res = pd.DataFrame(list(df), columns=['name'])

        for q in query_target_dict:
            df_query = df.query(queries[q])

            for t in query_target_dict[q]:
                if verbose_clear:
                    clear_output()
                print('query', q, 'target', t)
                for i in tqdm(df.drop(columns=ignore_features).columns): #, disable=not verbose):

                    if i in ignore_features:
                        continue

                    try:
                        optb = OptimalBinning(**binning_params)
                        optb.fit(df[i], df[t])
                        binning_table = optb.binning_table
                        binning_table.build()
                        res.loc[res['name'] == i, 'IV_' + str(q) + '_' + str(t)] = binning_table.iv
                    except Exception as inst:
                        res.loc[res['name'] == i, 'IV_' + str(q) + '_' + str(t)] = np.nan


        return res


#     @staticmethod
#     def calc_gini_catboost(args):
#         queries = args['queries']
#         pairs = args['pairs']
#         df = args['df']
#         targets = args['targets']

#         res = pd.DataFrame(list(df), columns=['name'])
#         for pair in pairs:
#             from_p = list(pair)[0]
#             to_p = pair[from_p]

#             for t in targets:
#                 for i in tqdm(df.columns):
#                     try:
#                         print()
#                         x_train = df.query(queries[from_p])
#                         x_train['const'] = 1
#                         x_train = x_train[[i, 'const']]

#                         y_train = df.query(queries[from_p])[t]

#                         x_test = df.query(queries[to_p])
#                         x_test['const'] = 1
#                         x_test = x_test[[i, 'const']]

#                         y_test = df.query(queries[to_p])[t]
#                         print(x_train.shape)
#                         print(y_train.shape)
#                         print(x_test.shape)
#                         print(y_test.shape)

#                         cb = CatBoostClassifier(eval_metric='NormalizedGini:hints=skip_train~false', logging_level='Silent',random_seed = 777,loss_function= 'Logloss',thread_count= -1).fit(x_train, y_train)
#                         gini_train = abs(2*roc_auc_score(y_train,cb.predict_proba(x_train)[:,1])-1)
#                         gini_test = abs(2*roc_auc_score(y_test,cb.predict_proba(x_test)[:,1])-1)

#                         res.loc[res['name'] == i, 'gini_catboost_train_' + str(from_p) + '_' + str(t)] = gini_train
#                         res.loc[res['name'] == i, 'gini_catboost_test_' + str(from_p) + '_' + str(t)] = gini_test
#                     except Exception as inst:
#                         print(t, i, inst)
#                         res.loc[res['name'] == i, 'gini_catboost_train_' + str(from_p) + '_' + str(t)] = np.nan
#                         res.loc[res['name'] == i, 'gini_catboost_test_' + str(from_p) + '_' + str(t)] = np.nan

#         return res