from tqdm import tqdm
from pandas.api.types import is_numeric_dtype

class SimpleFillEmptyValue:
    
    def __init__(self, sa, fill_median_border=5, fill_not_ok_border=75, verbose=False):
        '''
        Initialized simple fill empty

        Parameters :
        -------------
        sa : SimpleAnalysis object with DataFrame called by sa.research method
        datasets : DataFrame
            Dict with pair - 'source':'dataset'

        '''
        self.research = sa.research
        self.fill_median_border = fill_median_border
        self.fill_not_ok_border = fill_not_ok_border
        self.ok_comment = f'OK, not need fill(percent = 0)'
        self.ok_comment2 = f'OK, fill median(percent < {fill_median_border})'
        self.need_binning_comment = f'warning, need binning({fill_median_border} < percent < {fill_not_ok_border}))'
        self.need_drop_comment = f'warning, need drop(percent > {fill_not_ok_border})' 
        
        @property
        def get_fill_median_border(self):
            return self._fill_median_border 
        @property
        def set_fill_median_border(self):
            return self._fill_median_border
        
        @property
        def get_fill_not_ok_border(self):
            return self._fill_not_ok_border
        @property
        def set_fill_not_ok_border(self):
            return self._fill_not_ok_border         

        
        @property
        def get_ok_comment(self):
            return self._ok_comment 
        @property
        def set_ok_comment(self):
            return self._ok_comment

        @property
        def get_ok_comment2(self):
            return self._ok_comment2
        @property
        def set_ok_comment2(self):
            return self._ok_comment2
        
        @property
        def get_need_binning_comment(self):
            return self._need_binning_comment
        @property
        def set_need_binning_comment(self):
            return self._need_binning_comment  
        
        @property
        def get_need_drop_comment(self):
            return self._need_drop_comment
        @property
        def set_need_drop_comment(self):
            return self._need_drop_comment 
        

        
        for f_name in tqdm(self.research['name'], disable=not verbose):
            nan_percent_value = list(self.research.loc[(self.research['name'] == f_name), 'nan_percent'])[0]

            if nan_percent_value == 0: # если равно 0
                self.research.loc[(self.research['name'] == f_name), 'commentary_fill_nan'] = f'OK, not need fill(percent = 0)'
            elif nan_percent_value <= fill_median_border: # если меньше первой границы, то заменяем средним и модой
                self.research.loc[(self.research['name'] == f_name), 'commentary_fill_nan'] = f'OK, fill median(percent < {fill_median_border})'
            elif nan_percent_value > fill_median_border and nan_percent_value <= fill_not_ok_border: # если между первой и второй то говорим что надо бинить
                self.research.loc[(self.research['name'] == f_name), 'commentary_fill_nan'] = f'warning, need binning({fill_median_border} < percent < {fill_not_ok_border}))'
            elif nan_percent_value > fill_not_ok_border: # если больше верхней, то говорим что надо глядеть и мб удалять
                self.research.loc[(self.research['name'] == f_name), 'commentary_fill_nan'] = f'warning, need drop(percent > {fill_not_ok_border})'        
        
    def get_dict_by_comment(self, comment):
        feature_to_bins = {}

        for ds_name in self.research['source'].unique():
            feature_to_bins[ds_name] = list(self.research[
                (self.research['commentary_fill_nan'] == comment) & 
                (self.research['source'] == ds_name)
            ]['name'])
        
        return feature_to_bins
    
    def get_feature_dictionaries(self):
        ok_features = {}

        for ds_name in self.research['source'].unique():
            ok_features[ds_name] = list(self.research[
                ((self.research['commentary_fill_nan'] == self.ok_comment2)|(self.research['commentary_fill_nan'] == self.ok_comment)) & 
                (self.research['source'] == ds_name)
            ]['name'])
            
        features_to_be_binned = {}

        for ds_name in self.research['source'].unique():
            features_to_be_binned[ds_name] = list(self.research[
                (self.research['commentary_fill_nan'] == self.need_binning_comment) & 
                (self.research['source'] == ds_name)
            ]['name'])  
            
        features_to_be_dropped = {}

        for ds_name in self.research['source'].unique():
            features_to_be_dropped[ds_name] = list(self.research[
                (self.research['commentary_fill_nan'] == self.need_drop_comment) & 
                (self.research['source'] == ds_name)
            ]['name'])   
            
        self.ok_features = ok_features
        self.features_to_be_binned = features_to_be_binned
        self.features_to_be_dropped = features_to_be_dropped
        
        return ok_features, features_to_be_binned, features_to_be_dropped
    
    def get_feature_lists(self):
        feats_to_bin = []
        for i in self.features_to_be_binned.keys():
            feats_to_bin = feats_to_bin+self.features_to_be_binned[i]
        feats_to_bin 
        
        feats_ok = []
        for i in self.ok_features.keys():
            feats_ok = feats_ok+self.ok_features[i]
        feats_ok
        
        feats_to_drop = []
        for i in self.features_to_be_dropped.keys():
            feats_to_drop = feats_to_drop+self.features_to_be_dropped[i]
        return feats_ok, feats_to_bin, feats_to_drop
        
    def fill_empty(self, fill_median_border=5, fill_not_ok_border=75, verbose=False):
        self.fill_median_border = fill_median_border
        self.fill_not_ok_border = fill_not_ok_border
        self.ok_comment = f'OK, not need fill(percent = 0)'
        self.ok_comment2 = f'OK, fill median(percent < {fill_median_border})'
        self.need_binning_comment = f'warning, need binning({fill_median_border} < percent < {fill_not_ok_border}))'
        self.need_drop_comment = f'warning, need drop(percent > {fill_not_ok_border})' 
        
        for f_name in tqdm(self.research['name'], disable=not verbose):
            nan_percent_value = list(self.research.loc[(self.research['name'] == f_name), 'nan_percent'])[0]

            if nan_percent_value == 0: # если равно 0
                self.research.loc[(self.research['name'] == f_name), 'commentary_fill_nan'] = f'OK, not need fill(percent = 0)'
            elif nan_percent_value <= fill_median_border: # если меньше первой границы, то заменяем средним и модой
                self.research.loc[(self.research['name'] == f_name), 'commentary_fill_nan'] = f'OK, fill median(percent < {fill_median_border})'
            elif nan_percent_value > fill_median_border and nan_percent_value <= fill_not_ok_border: # если между первой и второй то говорим что надо бинить
                self.research.loc[(self.research['name'] == f_name), 'commentary_fill_nan'] = f'warning, need binning({fill_median_border} < percent < {fill_not_ok_border}))'
            elif nan_percent_value > fill_not_ok_border: # если больше верхней, то говорим что надо глядеть и мб удалять
                self.research.loc[(self.research['name'] == f_name), 'commentary_fill_nan'] = f'warning, need drop(percent > {fill_not_ok_border})'
                
                
                
    def get_summary(self):
        return self.research[['commentary_fill_nan', 'source','name']].groupby(['commentary_fill_nan', 'source']).count()                