import pandas as pd
from sklearn.model_selection import train_test_split
# from catboost import CatBoostClassifier
# import catboost
from sklearn.metrics import classification_report, roc_auc_score
import datetime
import scorecardpy
from IPython.display import clear_output
import numpy as np
import catboost
from catboost import CatBoostClassifier, Pool, cv
import pickle
import datetime as dt
pd.set_option('display.max_columns',None)
pd.set_option('display.max_rows',1500)
import tqdm
import featurewiz
from featurewiz import featurewiz

import optbinning as opt
from scipy import stats
from scipy.stats import spearmanr
from scipy.stats import pearsonr

import pandas as pd
import json
import datetime
import numpy as np
import os
from scipy.stats import ks_2samp

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import datetime
import pickle
import math
import re
import datetime as dt
from IPython.display import clear_output
import cx_Oracle

import catboost
from catboost import CatBoostClassifier
from sklearn.linear_model import LogisticRegression,LogisticRegressionCV
from sklearn.metrics import roc_auc_score,f1_score,precision_score,recall_score,make_scorer,roc_curve
from sklearn.model_selection import train_test_split,GridSearchCV,RandomizedSearchCV
from sklearn.model_selection import PredefinedSplit,KFold, StratifiedKFold
from sklearn.model_selection import cross_val_score
from statsmodels.stats.outliers_influence import variance_inflation_factor
import seaborn as sns
import statsmodels.api as sm

import shap
import scorecardpy
import xlsxwriter
from dateutil.relativedelta import relativedelta

import optbinning
from statsmodels.tools.tools import add_constant
from statsmodels.stats.outliers_influence import variance_inflation_factor
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

from optbinning import BinningProcess
from optbinning import Scorecard
from optbinning import OptimalBinning
from optbinning.scorecard import ScorecardMonitoring
from optbinning.scorecard import plot_auc_roc, plot_cap, plot_ks

import tqdm

import warnings
warnings.filterwarnings("ignore")

clear_output(wait=False)


def get_rid_of_nans(dataset_tt, feature_list, nans_percent=80, application_month_column='APPLICATION_MONTH',
                   set_type_col='set_type', train_set_name='train', test_set_name='test'):
    norm_features = []
    bad_features = []
    train_df = dataset_tt[(dataset_tt[set_type_col]==train_set_name)].reset_index(drop=True)
    test_df = dataset_tt[(dataset_tt[set_type_col]==test_set_name)].reset_index(drop=True)
    
    months = pd.DataFrame(dataset_tt[application_month_column].unique(),columns=['months'])['months'].apply(lambda x: str(x)[:-9])
    for i in months:
        missings = dataset_tt[dataset_tt[application_month_column]==i][feature_list].isna().sum()/dataset_tt[dataset_tt[application_month_column]==i][feature_list].shape[0]*100
        norm_features+=missings[missings<=nans_percent].index.tolist()
        bad_features+=missings[missings>nans_percent].index.tolist()   
        
    months = pd.DataFrame(train_df[application_month_column].unique(),columns=['months'])['months'].apply(lambda x: str(x)[:-9])
    for i in months:
        missings = train_df[train_df[application_month_column]==i][feature_list].isna().sum()/train_df[train_df[application_month_column]==i][feature_list].shape[0]*100
        norm_features+=missings[missings<=nans_percent].index.tolist()
        bad_features+=missings[missings>nans_percent].index.tolist()
        
    months = pd.DataFrame(test_df[application_month_column].unique(),columns=['months'])['months'].apply(lambda x: str(x)[:-9])
    for i in months:
        missings = test_df[test_df[application_month_column]==i][feature_list].isna().sum()/test_df[test_df[application_month_column]==i][feature_list].shape[0]*100
        norm_features+=missings[missings<=nans_percent].index.tolist()
        bad_features+=missings[missings>nans_percent].index.tolist()   
        
    return list(set(norm_features)), list(set(bad_features))
#     return norm_features

def psi_report_manually(df, 
                        feature_list, 
                        out_xlsx='psi_report_manual',
                        set_type_col='set_type', train_set_name='train', test_set_name='test', oot_set_name='oot',
                        app_month='APPLICATION_MONTH', app_month_datetime=True):
    """
    *Function description*:
    Вспомогательная функция получения psi-отчёта для уже разбененных переменных.
        
    *Parameters*:   
    -df (pandas.DataFrame): исходный датафрейм.
    
    -feature_list (list): список переменных.
    
    -out_xlsx (str): название xlsx файла, в который сохраняется psi_report. По умолчанию: 'psi_report'
    
    -app_month (str): название столбца, содержащего месяц даты заявки. По умолчанию: 'APPLICATION_MONTH'.
    
    -app_month_datetime (bool): Приведен ли столбец app_month к формату datetime. По умолчанию: True. 
        
    *Returns*: 
    -
    На выходе получаем отчет в формате xlsx, сохраненный в файл out_xlsx.
    """              
#     print(feature_list,df[feature_list],app_month,df[app_month])
    df = df[feature_list+[app_month]]
    if app_month_datetime==True:
        pass
    else:
        try:
            df[app_month] = pd.to_datetime(df[app_month], format='%Y-%m-%d', errors='raise')
            df[app_month] = df[app_month].str.slice(0,7)
        except ValueError:
            print(f'Format for {app_month} must be %Y-%m-%d')
            return
    
    feature_list = feature_list.copy()
    months = sorted(list(df[app_month].unique()))
#     print(months)
    
    for n,month in enumerate(months):
#         print(n)
        if n==0:
            base_df = df[df[set_type_col].isin([train_set_name])]
#             display(base_df)
#             print(months[(n-1)*3],months[(n-1)*3+1],[months[(n-1)*3+2]])
        try:
            months[n*3+2]

            psi_df_check = df[df[app_month].isin([months[n*3], 
                                                  months[n*3+1], 
                                                  months[n*3+2]
                                                 ])
                             ]
            if n==0:   
                period_psi = str(min(psi_df_check[app_month]))[:7] + '-' + str(max(psi_df_check[app_month]))[:7]
                base_df_period = str(min(base_df[app_month]))[:7] + '-' + str(max(base_df[app_month]))[:7]
                d01 = pd.DataFrame()
                for feat in feature_list:
                    df01 = get_psi_df(psi_df_check, 
                                      feat, 
                                      app_month=app_month)
                    d01 = d01.append(df01)
                d02 = d01

            else:
                period_psi = str(min(psi_df_check[app_month]))[:7] + '-' + str(max(psi_df_check[app_month]))[:7]
                d01 = pd.DataFrame()
                for feat in feature_list:
                    df01 = get_psi_df(psi_df_check, 
                                      feat, 
                                      app_month=app_month)
                    d01 = d01.append(df01)   
                d02 = d02.merge(d01, on=['col_name','woebin_val'], how='outer')

        except:
            break
    try:
        if len(months)%3==1:

                psi_df_check = df[df[app_month].isin(months[-1:])]

                period_psi = str(max(psi_df_check[app_month]))[:7]
                d01 = pd.DataFrame()
#                 print(feature_list)
                for feat in feature_list:
#                     print(feat)
                    df01 = get_psi_df(psi_df_check, 
                                      feat, 
                                      app_month=app_month)
                    d01 = d01.append(df01)   
                d02 = d02.merge(d01, on=['col_name','woebin_val'], how='outer')

        if len(months)%3==2:
                psi_df_check = df[df[app_month].isin(months[-2:])]
                period_psi = str(min(psi_df_check[app_month]))[:7] + '-' + str(max(psi_df_check[app_month]))[:7]
                d01 = pd.DataFrame()
                for feat in feature_list2:
                    df01 = get_psi_df(psi_df_check, 
                                      feat, 
                                      app_month=app_month)
                    d01 = d01.append(df01)   
                d02 = d02.merge(d01, on=['col_name','woebin_val'], how='outer')
                
    except:
        
        if len(months)%3==1:    
            psi_df_check = df[df[app_month].isin([months[(n-1)*3],     
                                                  months[(n-1)*3+1], 
                                                  months[(n-1)*3+2],
                                                  months[(n-1)*3+3]
                                                 ])
                             ]
            if n==0:   
                period_psi = str(min(psi_df_check[app_month]))[:7] + '-' + str(max(psi_df_check[app_month]))[:7]
                d01 = pd.DataFrame()
                for feat in feature_list:
                    df01 = get_psi_df(psi_df_check, 
                                      feat, app_month=app_month)
                    d01 = d01.append(df01)
                d02 = d01

            else:
                period_psi = str(min(psi_df_check[app_month]))[:7] + '-' + str(max(psi_df_check[app_month]))[:7]
                d01 = pd.DataFrame()
                for feat in feature_list:
                    df01 = get_psi_df(psi_df_check, 
                                      feat,
                                      app_month=app_month)
                    d01 = d01.append(df01)   
    #                 display(d02)
                d02 = d02.merge(d01,on=['col_name','woebin_val'], how='outer')

        if len(months)%3==2:    
            psi_df_check = df[df[app_month].isin([months[(n-1)*3],     
                                                  months[(n-1)*3+1], 
                                                  months[(n-1)*3+2],
                                                  months[(n-1)*3+3],
                                                  months[(n-1)*3+4]
                                                 ])
                             ]
            if n==0:   
                period_psi = str(min(psi_df_check[app_month]))[:7] + '-' + str(max(psi_df_check[app_month]))[:7]
                d01 = pd.DataFrame()
                for feat in feature_list:
                    df01 = get_psi_df(psi_df_check, 
                                      feat, 
                                      app_month=app_month)
                    d01 = d01.append(df01)
                d02=d01

            else:
                period_psi = str(min(psi_df_check[app_month]))[:7] + '-' + str(max(psi_df_check[app_month]))[:7]
                d01 = pd.DataFrame()
                for feat in feature_list:
                    df01 = get_psi_df(psi_df_check, 
                                      feat,
                                      app_month=app_month)
                    d01 = d01.append(df01)   
    #                 display(d02)
                d02 = d02.merge(d01, on=['col_name','woebin_val'], how='outer')  
        
    d02 = d02[['col_name']\
              +['woebin_val']\
              +[col for col in d02.columns if ' N' in col]\
              +[col for col in d02.columns if '_%' in col]
             ].reset_index(drop=True)
    

    for n,feat in enumerate([col[:-2] for col in d02.columns if '_%' in col]):
        if n==0:
#             print(d02.columns),print(base_df_period),print(feat)
            psi = psi_count_manual(d02, base_df_period, feat) 
        else:
            psi = psi_count_manual(psi, base_df_period, feat) 
      
    
    base_columns = ['col_name', 'woebin_val']
    num_columns = [col for col in psi.columns if ' N' in col]
    percent_columns = [col for col in psi.columns if '_%' in col]
    psi_percent_columns = [col for col in psi.columns if ('PSI%' in col)]
    
    format_dict = {feat:"{:.2f}" for feat in percent_columns + psi_percent_columns}
        
        
    psi_res_table = psi[base_columns+num_columns\
                        +percent_columns+psi_percent_columns]      
    return psi_res_table,psi_percent_columns

def psi_report(df, 
               target,                
               feature_list,
               feature_list_transformed,
               monitoring, 
               binning_process,
               already_binned_features=None, 
               app_month='APPLICATION_MONTH',
               set_type_col='set_type', train_set_name='train', test_set_name='test', oot_set_name='oot',
               out_xlsx='psi_report', app_month_datetime=True
              ):
    """
    *Function description*:
    Вспомогательная функция получения psi-отчёта.
        
    *Parameters*:   
    -target (str): название столбца с таргетом/таргета.
    
    -feature_list (list): список переменных.
    
    -monitoring (optbinning.scorecard.monitoring.ScorecardMonitoring): элемент optbinning-а, посредством которого можно производить psi-мониторинг по бинам переменных.
    
    -already_binned_features (list): список уже разбиненных переменных.
    
    -app_month (str): название столбца, содержащего месяц даты заявки. По умолчанию: 'APPLICATION_MONTH'.
    
    -out_xlsx (str): название xlsx файла, в который сохраняется psi_report. По умолчанию: 'psi_report'
    
    -app_month_datetime (bool): Приведен ли столбец app_month к формату datetime. По умолчанию: True. 
        
    *Returns*: 
    -
    На выходе получаем отчет в формате xlsx, сохраненный в файл out_xlsx.
    """          
    df = df[~df[target].isna()].drop_duplicates().reset_index(drop=True) #mask returns copy already
    
    if app_month_datetime==True:
        pass
    else:    
        try:
            df[app_month] = pd.to_datetime(df[app_month], format='%Y-%m-%d', errors='raise')
#             df[app_month] = df[app_month].str.slice(0,7)
        except ValueError:
            print(f'Format for {app_month} must be %Y-%m-%d')
            return
    
#     target = target.copy()
    months = sorted(list(df[app_month].unique()))
        
    if already_binned_features:
        for feat in already_binned_features:
            df[feat] = df[feat].fillna(-1) # filling nans with -1 doesnt change psi
                
                                                                                    
    for n,month in enumerate(df[app_month]):
        if n==0:
            base_df = df[df[set_type_col].isin([train_set_name])]
        try:
            months[n*3+2] # fast check

            psi_df_check = df[df[app_month].isin([months[n*3], 
                                                  months[n*3+1], 
                                                  months[n*3+2]
                                                 ])
                             ]
            if n==0:   
                psi = psi_mon(monitoring, binning_process,
                              base_df[feature_list+[app_month]], base_df[target], 
                              psi_df_check[feature_list+[app_month]], psi_df_check[target],
                              feature_list,
                              first=True,
                              app_month=app_month)
                psi_fin = psi
                # display(psi_fin)
            else:
                psi = psi_mon(monitoring, binning_process,
                              base_df[feature_list+[app_month]],base_df[target], 
                              psi_df_check[feature_list+[app_month]], psi_df_check[target], 
                              feature_list,
                              first=False,
                              app_month=app_month)
                psi_fin = psi_fin.merge(psi, on=['Variable','Bin'], how='outer')
                # display(psi_fin)
        except:
            # print(n)
            break
    
    try:
        if len(months)%3==1:
            psi_df_check = df[df[app_month].isin(months[-1:])]
#             print(base_df[app_month],psi_df_check[app_month])
            psi = psi_mon(monitoring, binning_process,
                          base_df[feature_list+[app_month]], base_df[target], 
                          psi_df_check[feature_list+[app_month]], psi_df_check[target], 
                          feature_list,
                          first=False,
                          app_month=app_month)
            psi_fin = psi_fin.merge(psi, on=['Variable','Bin'], how='outer')

        if len(months)%3==2:
            psi_df_check = df[df[app_month].isin(months[-2:])]

            psi = psi_mon(monitoring, binning_process,
                          base_df[feature_list+[app_month]],base_df[target], 
                          psi_df_check[feature_list+[app_month]], psi_df_check[target], 
                          feature_list,
                          first=False,
                          app_month=app_month)
            psi_fin = psi_fin.merge(psi, on=['Variable','Bin'], how='outer')
                
    except:
        # this exception occurs when there are not enough data in some bins for optbinning
        # solution: add more months
        
        if len(months)%3==1:    
            psi_df_check = df[df[app_month].isin([months[(n-1)*3],     
                                                  months[(n-1)*3+1], 
                                                  months[(n-1)*3+2],
                                                  months[(n-1)*3+3]
                                                 ])
                             ]
            if n==0:   
                psi = psi_mon(monitoring, binning_process,
                              base_df[feature_list+[app_month]], base_df[target], 
                              psi_df_check[feature_list+[app_month]], psi_df_check[target], 
                              feature_list,
                              first=True,
                              app_month=app_month)
                psi_fin = psi
                # display(psi_fin)
            else:
                psi = psi_mon(monitoring, binning_process,
                              base_df[feature_list+[app_month]], base_df[target], 
                              psi_df_check[feature_list+[app_month]], psi_df_check[target], 
                              feature_list,
                              first=False,
                              app_month=app_month)
                psi_fin = psi_fin.merge(psi, on=['Variable','Bin'], how='outer')
                # display(psi_fin)
                
        if len(months)%3==2:    
            psi_df_check = df[df[app_month].isin([months[(n-1)*3],     
                                                  months[(n-1)*3+1], 
                                                  months[(n-1)*3+2],
                                                  months[(n-1)*3+3],
                                                  months[(n-1)*3+4]
                                                 ])
                             ]
            if n==0:   
                psi = psi_mon(monitoring, binning_process,
                              base_df[feature_list+[app_month]], base_df[target], 
                              psi_df_check[feature_list+[app_month]], psi_df_check[target], 
                              feature_list,
                              first=True,
                              app_month=app_month)
                psi_fin = psi
                # display(psi_fin)
            else:
                psi = psi_mon(monitoring, binning_process,
                              base_df[feature_list+[app_month]], base_df[target], 
                              psi_df_check[feature_list+[app_month]], psi_df_check[target], 
                              feature_list,
                              first=False,
                              app_month=app_month)
                psi_fin = psi_fin.merge(psi, on=['Variable','Bin'], how='outer')
                # display(psi_fin)                    
        
    base_columns = [i for i in psi_fin.columns if 'base' in i]
    num_columns = [i for i in psi_fin.columns if ('base' not in i)&('N' in i)] 
    percent_columns = [i for i in psi_fin.columns if ('base' not in i)&('PSI%' not in i)&('%' in i)]
    psi_percent_columns = [i for i in psi_fin.columns if ('PSI%' in i)]
    # psi_fin = psi_fin.T.drop_duplicates().T
    
    format_dict = {feat:"{:.2f}" for feat in [base_columns[1]]+percent_columns\
                                              +psi_percent_columns
                  } 

    psi_res_table = psi_fin[['Variable', 'Bin']\
                            +base_columns+num_columns\
                            +percent_columns+psi_percent_columns\
                           ]
    
    psi_res_tab_copy = psi_res_table.copy()
    
    
    bins_guide = pd.DataFrame()
    for some_f in feature_list:
        interim_table = binning_process.get_binned_variable(some_f).binning_table.build()\
        [binning_process.get_binned_variable(some_f).binning_table.build().index!='Totals']\
        [binning_process.get_binned_variable(some_f).binning_table.build()['Bin']!='Special'][['Bin','Event rate','WoE', 'Count']]
        interim_table['Var'] = some_f

        bins_guide = bins_guide.append(interim_table)
#         display(bins_guide)
    monitoring_df = monitoring.psi_variable_table(style="detailed").rename(columns={'Bin':'Final_bin'})
    bins_guide = bins_guide.merge(monitoring_df[['Variable','Final_bin','Count E']],left_on=['Var','Count'],right_on=['Variable','Count E'],how='left').drop(columns=['Variable','Count E'])
    for some_f in feature_list:
        mon_vals = monitoring_df[(monitoring_df['Variable']==some_f)]['Final_bin'].values.tolist()
        bins_guide_vals = bins_guide[(bins_guide['Var']==some_f)]['Final_bin'].values.tolist()
        for fin_bin_name in mon_vals:
            if fin_bin_name not in bins_guide_vals:
                bins_guide.loc[(bins_guide['Var']==some_f)&(bins_guide['Final_bin'].isna()),'Final_bin'] = fin_bin_name
    
    bin_info = bins_guide[['Var','Bin','Final_bin']].rename(columns = {'Bin':'Origin_bin'})
    
    psi_res_tab_copy = psi_res_tab_copy.merge(bin_info,left_on=['Variable', 'Bin'],right_on=['Var','Final_bin'],how='left').drop(columns=['Final_bin','Var'])
    
    psi_res_tab_copy = psi_res_tab_copy[['Variable', 'Origin_bin', 'Bin']\
                            +base_columns+num_columns\
                            +percent_columns+psi_percent_columns\
                           ]\
                    .style\
                    .format(format_dict)\
                    .background_gradient(cmap='Reds', 
                                         axis=None,
                                         subset=percent_columns[:])\
                    .applymap(lambda x: f"color: {'red' if x>20 else 'black'}",
                              subset=psi_percent_columns)
    

    
    writer = pd.ExcelWriter(f'{out_xlsx}_test.xlsx', engine='xlsxwriter')
    psi_res_tab_copy.to_excel(writer, sheet_name='distrib_and_psi',index=False)
    bins_guide.to_excel(writer, sheet_name='bins_guide',index=False)
    writer.save()   
    
    return psi_res_table,psi_percent_columns
#                     .style\
#                     .format(format_dict)\
#                     .background_gradient(cmap='Reds', 
#                                          axis=None,
#                                          subset=percent_columns[:])\
#                     .applymap(lambda x: f"color: {'red' if x>20 else 'black'}",
#                               subset=psi_percent_columns)
    
    
#     writer = pd.ExcelWriter(f'{out_xlsx}.xlsx', engine='xlsxwriter')
#     psi_res_table.to_excel(writer, sheet_name='distrib_and_psi',index=False)
#     writer.save()    



def calc_psi_opt(df, 
                 feature_list, 
                 target, 
                 app_month = 'APPLICATION_MONTH',
                 categorical_features=[], 
                 flags_list=None, 
                 already_binned_features=None, 
                 set_type='set_type',
                 train_set_name='train',
                 test_set_name='test', 
                 oot_set_name='oot',
                 custom_dictionary={}                 
                 ):  
    """
    *Function description*:
    Фукнкция расчета psi по списку переменных.
        
    *Parameters*:    
    -df (pandas.DataFrame): исходный датафрейм.
    
    -feature_list (list): список переменных.
    
    -app_month (str): название столбца, содержащего дату (год, месяц) заявки.
    
    -categorical_features (list): Список категориальных переменных. По умолчанию: []. 
    
    -flags_list (list): Список переменных-флагов. По умолчанию: None.
    
    -already_binned_features (list): Список уже резбиненных переменных, не требующих бининга (может содержать переменные из предыдущих 2х списков). По умолчанию: None. 
    
    -set_type (str): Столбец датафрейма, содержащий разметку сэмплов по типу выборки (train/test/oot) По умолчанию: 'set_type'.
    
    -train_set_name текстовое значение столбца датафрейма, содержащее разметку сэмплов по типу выборки (train/test/oot), соответствующее сэмплам выборки train. По умолчанию: 'train'.
    
    -test_set_name (str): текстовое значение столбца датафрейма, содержащее разметку сэмплов по типу выборки (train/test/oot), соответствующее сэмплам выборки test. По умолчанию: 'test'.
        
    -oot_set_name (str): текстовое значение столбца датафрейма, содержащее разметку сэмплов по типу выборки (train/test/oot), соответствующее сэмплам выборки oot. По умолчанию: 'oot'.
        
    -custom_dictionary (dict): По умолчанию: {}. Представляет из себя словарь, ключами которого являются названия переменных, а значениями - словари. 
        В словаре-значении содержатся:
            -user_splits - массив значений, по которым происходит разбивка на бины; 
            -user_splits_fixed - массив значений True/False, соразмерный размеру user_splits, отмечающий обязательность выделения определенного набора значений в отдельный бин в случае значения == True 
            (все проставляю False, т.к. при True иногда выдает ошибку)
            -dtype - тип переменной (categorical или numerical), 
            -solver - алгоритм бининга (в tutorial использовался mip) 
            
    !!!Более подробно:
        http://gnpalencia.org/optbinning/tutorials/tutorial_binary.html     
        
    ***customer_dictionary EXAMPLE:
        {'LIFETIME_BIN': {'user_splits': array([[ 5.],[-1.],[ 4.],[ 3.],[ 2.],[ 0.],[ 1.]]),
                  'user_splits_fixed': [False, False, False, False, False, False, False],
                  'dtype': 'categorical',
                  'solver': 'mip'},
         'BLOCK_DUR': {'user_splits': array([[ 0.],[-1.],[ 1.],[ 4.],[ 2.],[ 3.],[ 5.]]),
                       'user_splits_fixed': [False, False, False, False, False, False, False],
                       'dtype': 'categorical',
                       'solver': 'mip'},
         'CIRCLE': {'user_splits': array([[ 5.],[-1.],[ 3.],[ 4.],[ 0.],[ 2.],[ 1.]]),
                    'user_splits_fixed': [False, False, False, False, False, False, False],
                    'dtype': 'categorical',
                    'solver': 'mip'},
         'SOURCES_BIN3': {'user_splits': array([[ 2.],[ 0.],[ 1.],[-1.]]),
                          'user_splits_fixed': [False, False, False, False],
                          'dtype': 'categorical',
                          'solver': 'mip'}}
        
    *Returns*: 
    -res (pandas.DataFrame): датафрейм с PSI по переменным на выборке train-test.
     
    -res2 (pandas.DataFrame): датафрейм с PSI по переменным на выборке oot.
    
    -monitoring2 (optbinning.scorecard.monitoring.ScorecardMonitoring): элемент optbinning-а, посредством которого можно производить psi-мониторинг по бинам переменных. 
    """           
    
    df = df[~df[target].isna()].copy()
    
    other_features = []    
    if flags_list:
        
        flags_dictionary = {feat:{"user_splits": np.array([[0], [1]])} for feat in flags_list}
        custom_dictionary.update(flags_dictionary)
        other_features = list(set(feature_list)-set(flags_list))
        
    if already_binned_features:
        other_features =  list(set(other_features)-set(already_binned_features))        
        test_dict = {}
        
        for feat in already_binned_features:
            
            df[feat] = df[feat].fillna(-1) if df[feat].isna().sum()>0.0 else df[feat].copy() 
            val_count = df[feat].value_counts()
            
            test_dict[feat]={'user_splits':np.array(np.array([[j] for j in val_count.index])),
                             'user_splits_fixed':[False for i in range(len(val_count))],
                             'dtype':"categorical", 
                             'solver':'mip',
                             'monotonic_trend': "auto_asc_desc"}
                
        custom_dictionary.update(test_dict)
        # print(custom_dictionary)
        
    if len(other_features)>=1:
        monotonic_trend_dict = {}    
        for feature in other_features:
            monotonic_trend_dict[feature]={'monotonic_trend': "auto_asc_desc"}
        custom_dictionary.update(monotonic_trend_dict)  
        
    cd = {}
    for fn in feature_list:
        cd[fn]={'metric_special':'empirical','metric_missing':"empirical"}        
    binning_process = BinningProcess(feature_list,
                                     selection_criteria={"iv": {"min": 0.1, "max": 1},
#                                                          "quality_score": {"min": 0.1}
                                                        },
                                     categorical_variables=categorical_features,
                                     fixed_variables=feature_list,
                                     min_bin_size=0.1,
                                     binning_fit_params=custom_dictionary,
                                     binning_transform_params=cd)
    
    estimator = LogisticRegression(solver="lbfgs")  
    binning_process.fit_transform(df[df[set_type]==train_set_name][feature_list], df[df[set_type]==train_set_name][target], check_input=False)
    f1 = feature_list
    f2 = list(map(lambda x: x+'_BINNED', f1))
    feature_list_transformed = f2
    dd2 = {}
    for n1,n2 in zip(f1,f2):
        dd2[n1] = n2
        
    transformed_feats = binning_process.transform(df[feature_list], check_input=False).rename(columns=dd2)
    df[transformed_feats.columns.tolist()] = transformed_feats
    scorecard = Scorecard(binning_process=binning_process,
                          estimator=estimator)

    scorecard.fit(binning_process.transform(df[df[set_type]==train_set_name][feature_list]),
                  df[df[set_type]==train_set_name][target], 
#                   metric_special="empirical", 
#                   metric_missing="empirical"
                 )
#     print(feature_list_transformed)    
    monitoring = ScorecardMonitoring(scorecard=scorecard, psi_method="cart",
                                     psi_n_bins=10, verbose=False)
    monitoring.fit(binning_process.transform(df[df[set_type]==test_set_name][feature_list]),
                   df[df[set_type]==test_set_name][target],
                   binning_process.transform(df[df[set_type]==train_set_name][feature_list]), 
                   df[df[set_type]==train_set_name][target])
    
    res = monitoring.psi_variable_table(style="summary")
    

    monitoring.fit(binning_process.transform(df[df[set_type]==oot_set_name][feature_list]),
                    df[df[set_type]==oot_set_name][target],
                    binning_process.transform(df[df[set_type].isin([train_set_name])][feature_list]), 
                    df[df[set_type].isin([train_set_name])][target])
    res2 = monitoring.psi_variable_table(style="summary")
    
    return res.set_index('Variable', drop=True), res2.set_index('Variable'), monitoring, scorecard, binning_process, feature_list_transformed, df

def get_psi_df(some_df, feature_name, app_month='APPLICATION_MONTH'):
    """
    *Function description*:
    Вспомогательная функция получения woebin-значений и psi по переменной.
        
    *Parameters*:    
    -some_df (pandas.DataFrame): исходный датафрейм.
    
    -feature_name (str): название переменной.
    
    -app_month (str): название столбца, содержащего месяц даты заявки.
        
    *Returns*: 
    -some_df (pandas.DataFrame): датафрейм добавленными столбцами woebin и psi
    """ 
    some_df = some_df[[feature_name, app_month]].copy()
    
    some_df[app_month] = some_df[app_month].astype(str)
    period_psi = min(some_df[app_month])[:-3]+'-'+max(some_df[app_month])[:-3]
    
    some_df = some_df[feature_name]\
           .value_counts(dropna=False)\
           .to_frame()\
           .assign(tmp=lambda some_df: some_df[feature_name] / some_df[feature_name].sum(),
                   col_name=lambda some_df: f'{feature_name}')\
           .rename(columns={feature_name: period_psi+' N',
                            'tmp':        period_psi+'_%'})\
           .rename_axis('woebin_val')\
           .reset_index()
    some_df['woebin_val'] =  some_df['woebin_val'].fillna('Missing')  
    some_df[period_psi+'_%'] = 100*some_df[period_psi+'_%'].round(4)
    return some_df

def psi_count_manual(psi_df, base_period, period_psi):
    """
    *Function description*:
    Вспомогательная функция получения woebin-значений и psi по переменной.
        
    *Parameters*:    
    -some_df (pandas.DataFrame): исходный датафрейм.
    
    -feature_name (str): название переменной.
    
    -app_month (str): название столбца, содержащего месяц даты заявки. По умолчанию: 'APPLICATION_MONTH'.
        
    *Returns*: 
    -some_df (pandas.DataFrame): датафрейм добавленными столбцами woebin и psi
    """ 
    psi_df[period_psi + '_PSI%'] = np.round((psi_df[period_psi+'_%'] - psi_df[base_period+'_%'])\
                                    *np.log(psi_df[period_psi+'_%'] / psi_df[base_period+'_%']),2)
    return psi_df

def psi_mon(monitoring, 
            binning_process,
            x_tr, y_tr, 
            x_tes, y_tes, 
            feature_list,
#             period,
            first=True,
            app_month='APPLICATION_MONTH'):
    """
    *Function description*:
    Вспомогательная функция получения .
        
    *Parameters*:   
    -monitoring (optbinning.scorecard.monitoring.ScorecardMonitoring): элемент optbinning-а, посредством которого можно производить psi-мониторинг по бинам переменных.
    
    -x_tr (pandas.DataFrame): train датасет с переменными.
    
    -y_tr (pandas.DataFrame): train датасет с таргетом.
    
    -x_tes (pandas.DataFrame): test датасет с переменными.
    
    -y_tes (pandas.DataFrame): test датасет с таргетом.
    
    -feature_list (list): список переменных.
    
    -first (bool): флаг, обозначающий, является ли прогон 1м. Вспомогательный флаг используется при формировании psi-отчета в цикле.
        
    *Returns*: 
    -monitoring_2: датафрейм с psi-мониторингом по бинам переменных.
    """     
    monitoring.fit(binning_process.transform(x_tes[feature_list]), y_tes, binning_process.transform(x_tr[feature_list]), y_tr)
    period = str(min(x_tes[app_month]))[:-12] + '-' + str(max(x_tes[app_month]))[:-12]
    
    if first==True:
        period = str(min(x_tr[app_month]))[:-12] + '-' + str(max(x_tr[app_month]))[:-12]        
        monitoring_2 = monitoring\
                        .psi_variable_table(style="detailed")\
                        .rename(columns={'Count A':      'N '+period,
                                         'Count A (%)':  '% '+period,
                                         'Count E':      'N base '+period,
                                         'Count E (%)':  '% base '+period,
                                         'PSI':          'PSI% '+period
                                        }
                               )
        monitoring_2['% base '+period] = 100 * monitoring_2['% base '+period].round(4)
        
    else:
        monitoring_2 = monitoring\
                        .psi_variable_table(style="detailed")\
                        .rename(columns={'Count A':      'N '+period,
                                         'Count A (%)':  '% '+period,
                                         'PSI':          'PSI% '+period
                                        }
                               )\
                        .drop(columns=['Count E (%)', 'Count E'])
        
    monitoring_2['PSI% '+period] = 100 * monitoring_2['PSI% '+period].round(4)
    monitoring_2['% '+period] = 100 * monitoring_2['% '+period].round(4)
    monitoring_2['Bin'] = monitoring_2['Bin'].astype(str)
    
    return monitoring_2


# import statsmodels.formula.api as sm
import statsmodels.api as sm
def backwardElimination(x, Y, sl, columns):
    numVars = len(x[0])
    for i in range(0, numVars):
        regressor_OLS = sm.OLS(Y, x, missing='drop').fit()
        maxVar = max(regressor_OLS.pvalues).astype(float)
        if maxVar > sl:
            for j in range(0, numVars - i):
                if (regressor_OLS.pvalues[j].astype(float) == maxVar):
#                     x = np.delete(x, j, 1)
                    columns = np.delete(columns, j)
                    
    print(regressor_OLS.summary())
    return columns

# SL = 0.05
# selected_columns = backwardElimination(df2[psi_norm].fillna(-9999).values, df2['GL30_06'].values, SL, psi_norm)

# %%time
# SL = 0.05
# selected_columns2 = backwardElimination(df2[psi_norm[:30]].values, df2['GL30_06'].values, SL, psi_norm)


def gini_stats_features(df, 
                        feature_list, 
                        target, 
                        psi_df_gen,
                        set_type='set_type',
                        train_set_name='train', 
                        test_set_name='test',
                        oot_set_name='oot',
#                         categorical_features=[],
#                         flags_list=None,
#                         already_binned_features = None,
#                         application_month_column_name = 'APPLICATION_MONTH',
#                         custom_dictionary={}
                       ):
    """
    *Function description*:
    Фукнкция расчета метрик gini, KS, VIF, IV, PSI по списку переменных.

    *Parameters*:    
    -df (pandas.DataFrame): исходный датафрейм.
    
    -feature_list (list): список переменных.        
    
    -target (str): название столбца датафрейма, в котором содержится таргет.
    
    -set_type (str): Столбец датафрейма, содержащий разметку сэмплов по типу выборки (train/test/oot) По умолчанию: 'set_type'.
    
    -train_set_name текстовое значение столбца датафрейма, содержащее разметку сэмплов по типу выборки (train/test/oot), соответствующее сэмплам выборки train. По умолчанию: 'train'.
    
    -test_set_name (str): текстовое значение столбца датафрейма, содержащее разметку сэмплов по типу выборки (train/test/oot), соответствующее сэмплам выборки test. По умолчанию: 'test'.
    
    -oot_set_name (str): текстовое значение столбца датафрейма, содержащее разметку сэмплов по типу выборки (train/test/oot), соответствующее сэмплам выборки oot. По умолчанию: 'oot'.        
    
    -categorical_features (list): Список категориальных переменных. По умолчанию: []. 
    
    -flags_list (list): Список переменных-флагов. По умолчанию: None.
    
    -already_binned_features (list): Список уже резбиненных переменных, не требующих бининга (может содержать переменные из предыдущих 2х списков). По умолчанию: None. 
    
    -application_month_column_name (str): название столбца, содержащего дату (год, месяц) заявки.

    -custom_dictionary (dict): По умолчанию: {}. Представляет из себя словарь, ключами которого являются названия переменных, а значениями - словари. 
        В словаре-значении содержатся:
            -user_splits - массив значений, по которым происходит разбивка на бины; 
            -user_splits_fixed - массив значений True/False, соразмерный размеру user_splits, отмечающий обязательность выделения определенного набора значений в отдельный бин в случае значения == True 
            (все проставляю False, т.к. при True иногда выдает ошибку)
            -dtype - тип переменной (categorical или numerical), 
            -solver - алгоритм бининга (в tutorial использовался mip) 

    !!!Более подробно:
        http://gnpalencia.org/optbinning/tutorials/tutorial_binary.html     

    -customer_dictionary EXAMPLE:
        {'LIFETIME_BIN': {'user_splits': array([[ 5.],[-1.],[ 4.],[ 3.],[ 2.],[ 0.],[ 1.]]),
                  'user_splits_fixed': [False, False, False, False, False, False, False],
                  'dtype': 'categorical',
                  'solver': 'mip'},
         'BLOCK_DUR': {'user_splits': array([[ 0.],[-1.],[ 1.],[ 4.],[ 2.],[ 3.],[ 5.]]),
                       'user_splits_fixed': [False, False, False, False, False, False, False],
                       'dtype': 'categorical',
                       'solver': 'mip'},
         'CIRCLE': {'user_splits': array([[ 5.],[-1.],[ 3.],[ 4.],[ 0.],[ 2.],[ 1.]]),
                    'user_splits_fixed': [False, False, False, False, False, False, False],
                    'dtype': 'categorical',
                    'solver': 'mip'},
         'SOURCES_BIN3': {'user_splits': array([[ 2.],[ 0.],[ 1.],[-1.]]),
                          'user_splits_fixed': [False, False, False, False],
                          'dtype': 'categorical',
                          'solver': 'mip'}}

    *Returns*: 
    -df_stats (pandas.DataFrame): датафрейм с метриками по переменным на выборке train-test.
    
    -df_stats_oot (pandas.DataFrame): датафрейм с метриками по переменным на выборке oot.
    
    -monitoring (optbinning.scorecard.monitoring.ScorecardMonitoring): элемент optbinning-а, посредством которого можно производить psi-мониторинг по бинам переменных. 
    """        

    df = df.copy()
    
    df_stats = pd.DataFrame(columns = ['gini_train','gini_test', 'KS_train', 'KS_test'])       
    df_stats_oot = pd.DataFrame(columns = ['gini_oot', 'KS_oot']) 
    
    algorithm = CatBoostClassifier(iterations=50, verbose=False, random_state=42)
    
    for col in feature_list:
        
        # Fill Train/Test
        df_stats.loc[col,'gini_train'], df_stats.loc[col,'gini_test'], \
             df_stats.loc[col,'KS_train'], df_stats.loc[col,'KS_test'] = \
                                        calc_gini_feature(algorithm, df, target, 
                                                          [col], 
                                                          df[df[set_type]==train_set_name].index, 
                                                          df[df[set_type]==test_set_name].index, 
                                                          ks=True)
        
#         df_stats.loc[col,'VIF'] = calc_vif(df[df[set_type].isin([train_set_name, test_set_name])][feature_list], 
#                                            col)
              
#         df_stats.loc[col,'IV'] = calc_iv(df[df[set_type].isin([train_set_name, test_set_name])], 
#                                          col, target)


        # Fill oot
#         df_stats_oot.loc[col,'gini_oot'], _, \
#             df_stats_oot.loc[col,'KS_oot'], __ = \
#                                                calc_gini_feature(algorithm, df, target,
#                                                                  [col], 
#                                                                  df[df[set_type]==oot_set_name].index, 
#                                                                  df[df[set_type]==test_set_name].index, 
#                                                                  ks=True)

#         df_stats_oot.loc[col,'VIF'] = calc_vif(df[(df[set_type]==oot_set_name)][feature_list], col)
#         df_stats_oot.loc[col,'IV'] = calc_iv(df[(df[set_type]==oot_set_name)], col, target)


#     psi_df, _, monitoring, scorecard, binning_process, feature_list_transformed, df_binned = calc_psi_opt(df, 
#                                                    feature_list, 
#                                                    target, 
#                                                    app_month=application_month_column_name,
#                                                    categorical_features=categorical_features, 
#                                                    flags_list=flags_list, 
#                                                    already_binned_features=already_binned_features,
#                                                    custom_dictionary=custom_dictionary,
#                                                    set_type=set_type,
#                                                    train_set_name=train_set_name,
#                                                    test_set_name=test_set_name, 
#                                                    oot_set_name=oot_set_name)

    psi_df_gen['PSI'] = np.round(psi_df_gen['PSI'].values*100,2)
#     psi_df_oot['PSI'] = np.round(psi_df_oot['PSI'].values*100,2)
    

    df_stats = df_stats.merge(psi_df_gen, left_index=True, right_index=True,
                              how='outer')    
    
#     df_stats_oot = df_stats_oot.merge(psi_df_oot, left_index=True, right_index=True,
#                                       how='outer')  
    
    return df_stats


def calc_gini_feature(alg, df, target, feature_list, train_ind, test_ind, ks=False):
    """
    *Function description*:
    Фукнкция расчета gini и ks по отдельным переменным.
        
    *Parameters*:    
    -alg (CatBoostClassifier/LogisticRegression): используемый алгоритм.
    
    -feature_list (list): список переменных, должна быть хотя бы 1 переменная.
        
    -train_ind (pandas.core.indexes): индексы обучающей выборки.
     
    -test_ind (pandas.core.indexes): индексы тестовой выборки.
        
    -ks (bool): флаг, активирующий расчет ks (если True).
        
    *Returns*: 
    -gini (%): метрика gini (умноженная на 100, округлённая до сотых)
    
    -ks.statistic (%): метрика KS (умноженная на 100, округлённая до сотых. Возвращется, если параметр ks==True)
    """
  

    alg = alg.fit(df.loc[train_ind, feature_list], df.loc[train_ind, target])
    
    alg_train = alg.predict_proba(df.loc[train_ind, feature_list])[:,1]
    alg_test  = alg.predict_proba(df.loc[test_ind, feature_list])[:,1]
    
    ks_train, gini_train = calc_gini_ks(df.loc[train_ind, target], alg_train)
    ks_test, gini_test  = calc_gini_ks(df.loc[test_ind, target], alg_test)
    
    if ks:
        return round(gini_train*100,2), round(gini_test*100,2), round(ks_train,2), round(ks_test,2)
    
    return round(gini_train*100,2), round(gini_test*100,2)

def calc_gini_ks(y_true, y_pred):
    """
    *Function description*:
    Функция расчета метрик gini и Колмогорова-Смирнова.
        
    *Parameters*:    
    -y_true (int):  target
    
    -y_pred (float):  probability

    *Returns*: 
    -gini (float): метрика gini (не умножалась на 100, не округлялась)
    
    -ks.statistic (float): метрика KS (не умножалась на 100, не округлялась)
    """
    
    # Объединяем таргет и предикт
    df = pd.DataFrame()
    df['real'] = y_true
    df['proba'] = y_pred
    
    # Фильтруем для расчета КС
    class0 = df[df['real'] == 0]
    class1 = df[df['real'] == 1]

    ks = ks_2samp(class0['proba'], class1['proba'])
    
    # Расчет Джини
    try:
        gini = 2*roc_auc_score(df['real'] , df['proba']) - 1
    except:
        gini = 0

    return gini, ks.statistic 

def calc_vif(df,var):
    """
    *Function description*:
    Фукнкция расчета vif по отдельной переменной.
        
    *Parameters*:    
    -df (pandas.DataFrame): исходный датафрейм.
    
    -var (str): название переменной.
        
    *Returns*: 
    -VIF (float): возвращает метрику VIF, округленную до 3го знака.
    """    
    
    X = add_constant(df)
    for i in X.columns:
        X.loc[:,i]=X.loc[:,i].fillna(X.loc[:,i].median())
    ind = list(X.columns).index(var)
    return round(variance_inflation_factor(X.values, ind),3)

def calc_iv(df, feature, target):
    """
    *Function description*:
    Фукнкция расчета iv по отдельной переменной.
        
    *Parameters*:    
    -df (pandas.DataFrame): исходный датафрейм.
        
    -feature (str): название переменной.
        
    -target (str): название столбца, содержащего таргет.
        
    *Returns*: 
    -iv (float): возвращает метрику IV, округленную до 2го знака.
    """       
    
    if df[feature].nunique()<=30:
        
        lst = []

        for i in range(df[feature].nunique()):
            val = list(df[feature].unique())[i]
            lst.append([feature, 
                        val, 
                        df[df[feature] == val].count()[feature], 
                        df[(df[feature] == val) & (df[target] == 1)].count()[feature]
                       ])

        data = pd.DataFrame(lst, columns=['Variable', 'Value', 'All', 'Bad'])
        data = data[data['Bad'] > 0]

        data['Share'] = data['All'] / data['All'].sum()
        data['Bad Rate'] = data['Bad'] / data['All']
        data['Distribution Good'] = (data['All'] - data['Bad']) / (data['All'].sum() - data['Bad'].sum())
        data['Distribution Bad'] = data['Bad'] / data['Bad'].sum()
        data['WoE'] = np.log(data['Distribution Good'] / data['Distribution Bad'])
        data['IV'] = (data['WoE'] * (data['Distribution Good'] - data['Distribution Bad'])).sum()

        data = data.sort_values(by=['Variable', 'Value'], ascending=True)

        return round(data['IV'].values[0],2)
        
    optb = OptimalBinning(name=feature, dtype="numerical", solver="cp", monotonic_trend="auto_asc_desc")
    optb.fit(df[feature].values, 
             df[target])
    
    df[f'{feature}_woe'] = optb.transform(df[feature], metric="woe")
    
    return calc_iv(df, f'{feature}_woe', target)

def l1_selection(train, long_list, target):
    train = train.copy()
    scaler = StandardScaler()
    scaler.fit(train[long_list].fillna(0))

    sel_ = SelectFromModel(LogisticRegression(penalty='l1', C=0.001, solver='saga', class_weight='balanced', n_jobs=-1))
    sel_.fit(scaler.transform(train[long_list].fillna(0)), train[target])

    selected_feat = train[long_list].columns[(sel_.get_support())]
    print(f'total features: {train[long_list].shape[1]}')
    print(f'selected features: {len(selected_feat)}')
    print(f'features with coefficients shrank to zero: {np.sum(sel_.estimator_.coef_ == 0)}')

    return list(selected_feat)


# !pip install tqdm
import tqdm

def pair_corr_counter(psi_norm,df2,df_len_for_pair_corr,corr_percent):   
    df2_corr = df2.sample(df_len_for_pair_corr, random_state=777).reset_index(drop=True)
    corr_pairs = []
    corr_df = pd.DataFrame()
    n=0
    from scipy import stats
    for i in tqdm.tqdm(psi_norm):
        for j in psi_norm:
            if (i,j) in corr_pairs or (j,i) in corr_pairs or i==j:
                pass

            else:
                corr_pairs.append((i,j))
                corr_pairs.append((j,i))
                corr_df.loc[n,'var1'] = i
                corr_df.loc[n,'var2'] = j
                a = df2[i].values
                b = df2[j].values
                
                nas = np.logical_or(pd.isnull(a),pd.isnull(b))

                if df2_corr[i].value_counts().shape[0]>100 and df2_corr[j].value_counts().shape[0]>100:
                    res = stats.pearsonr(a[~nas],b[~nas])
                    statistic = res[0]
                    pvalue = res[1]

                elif df2_corr[i].value_counts().shape[0]<=100 & df2_corr[j].value_counts().shape[0]<=100:
                    res = stats.spearmanr(a[~nas],b[~nas])
                    statistic = res[0]
                    pvalue = res[1]

                else:
                    res1 = stats.pearsonr(a[~nas],b[~nas])
                    res2 = stats.spearmanr(a[~nas],b[~nas])
                    if res1[0]>res2[0]:
                        statistic = res1[0]
                        pvalue = res1[1]
                    else:
                        statistic = res2[0]
                        pvalue = res2[1]        
                corr_df.loc[n,'r'] = statistic
                corr_df.loc[n,'p'] = pvalue
                n+=1
#                 print(n)
    high_corr_df = corr_df[corr_df['r']>corr_percent] 
    return corr_df, high_corr_df

def predotbor(full_df, f_longlist, nans_percent=80, psi_percent=15, corr_percent=0.85, nans_dropout_type='lite',\
              set_type_col='set_type', train_set_name='train', test_set_name='test', oot_set_name='oot',\
              target='GL30_06', application_month_column='APPLICATION_MONTH', categorical_features=[],\
              flags_list=None,already_binned_features=None,custom_dictionary={}, df_len_for_pair_corr=100000, months_list_to_drop=[]):
#     print(full_df.shape)
    full_df = full_df[~full_df[application_month_column].isin(months_list_to_drop)].reset_index(drop=True)
#     print(full_df.shape)    
    df2 = full_df[(full_df[set_type_col]==train_set_name)|(full_df[set_type_col]==test_set_name)].reset_index(drop=True)
    
    # Выбрасываем переменные по которым % миссингов более чем nans_percent хотя бы в 1 месяце на рассматриваемом периоде  
    print('1.Выявляем пропуски в данных')
    norm_features, bad_features = get_rid_of_nans(df2, f_longlist, nans_percent=nans_percent, application_month_column=application_month_column, 
                                                  set_type_col=set_type_col, train_set_name=train_set_name, test_set_name=test_set_name)
    if nans_dropout_type=='lite':
        bad_features = list(set(bad_features) - set(norm_features))                    
        nans_cols_len = len(bad_features)
        print(f'Исключили {nans_cols_len} столбцов, содержащих более {nans_percent}% миссингов')
        print() 
                     
    elif nans_dropout_type=='hard':
        norm_features = list(set(norm_features) - set(bad_features))                    
        nans_cols_len = len(bad_features)
        print(f'Исключили {nans_cols_len} столбцов, содержащих более {nans_percent}% миссингов')
        print()        
        
    # Считаем PSI по бинам поквартально   
    print('2.Считаем PSI по бинам поквартально')
    psi_df_gen,_, monitoring, scorecard, binning_process, feature_list_transformed, df_binned = calc_psi_opt(full_df, 
             norm_features, 
             target, 
             app_month = application_month_column,
             categorical_features=categorical_features, 
             flags_list=flags_list, 
             already_binned_features=already_binned_features, 
             set_type=set_type_col,
             train_set_name=train_set_name,
             test_set_name=test_set_name, 
             oot_set_name=oot_set_name,
             custom_dictionary=custom_dictionary                 
             )
    
    psi_df, psi_cols = psi_report(df_binned, target, 
                   norm_features,
                   feature_list_transformed,
                   monitoring, 
                   binning_process,
                   already_binned_features=already_binned_features, 
                   app_month=application_month_column,
                   set_type_col=set_type_col, 
                   train_set_name=train_set_name, 
                   test_set_name=test_set_name, 
                   oot_set_name=oot_set_name,                                  
                   # out_xlsx='psi_report', app_month_datetime=True
                  )    
    
    psi_df2 = psi_df[psi_cols+['Variable']].iloc[:,1:]
    psi_df2['mean'] = psi_df2[psi_df2.iloc[:,:-1].columns].mean(axis=1)
    psi_df2['max'] = psi_df2[psi_df2.iloc[:,:-1].columns].max(axis=1)
    psi_df2['min'] = psi_df2[psi_df2.iloc[:,:-1].columns].min(axis=1)
    psi_df2['razmah'] = psi_df2['max'] - psi_df2['min']

    psi_df3 = psi_df2.groupby('Variable')['max'].max()
    
    # Выбрасываем переменные по которым % psi более чем psi_percent хотя бы в 1м квартале на рассматриваемом периоде     
    psi_bad = psi_df3[psi_df3>psi_percent].index.tolist()
    psi_norm = psi_df3[psi_df3<=psi_percent].index.tolist()    
    print(f'Исключили {len(psi_bad)} переменных, по которым PSI выше {psi_percent}% хотя бы на 1м рассматриваемом периоде')
    print()
    # Формируем датафрейм с попарными корреляциями
    
    # минут 30 считает. Можно сделать на подвыборке меньшего размера
    # меньше на +-10 минут считает, еси до 10тыс сократить выборки. Мб имеет смысл до 5тыс сократить или до 3х
    print('3.Считаем парную корреляцию')
    
    corr_df, high_corr_df = pair_corr_counter(psi_norm,df2,df_len_for_pair_corr=df_len_for_pair_corr,corr_percent=corr_percent)
    
#     len(high_corr_df) 
    high_corred_flist_unique = list(set(high_corr_df['var1'].tolist()+high_corr_df['var2'].tolist()))
    print(f'Выявлено сильно скоррелированных переменных: {len(high_corred_flist_unique)} ')
    print()
    # Накат метрик
    print('4.Рассчитываем скоры для скоррелированных переменных')
    df_stats = gini_stats_features(full_df, 
                        high_corred_flist_unique, 
                        target, 
                        psi_df_gen,           
                        set_type=set_type_col,
                        train_set_name=train_set_name, 
                        test_set_name=test_set_name,
                        oot_set_name=oot_set_name,
#                         categorical_features=categorical_features,
#                         flags_list=flags_list,
#                         already_binned_features = already_binned_features,
#                         application_month_column_name = application_month_column,
#                         custom_dictionary=custom_dictionary
                                  )
    
    # Формирование флагов/метрик
    flags_df = pd.DataFrame()
    if len(high_corred_flist_unique)>0:
        for i in high_corred_flist_unique:
            flags_df.loc[i,'occurance_num'] = high_corr_df[high_corr_df['var1']==i].shape[0]+high_corr_df[high_corr_df['var2']==i].shape[0]
            flags_df.loc[i,'psi_maxval'] = psi_df3[psi_df3.index==i].max()

            flags_df.loc[i,'gini_train'] = df_stats[df_stats.index==i]['gini_train'].values[0]
            flags_df.loc[i,'gini_test'] = df_stats[df_stats.index==i]['gini_test'].values[0]
            flags_df.loc[i,'gini_sum'] = df_stats[df_stats.index==i]['gini_train'].values[0] + df_stats[df_stats.index==i]['gini_test'].values[0]

            flags_df.loc[i,'KS_train'] = df_stats[df_stats.index==i]['KS_train'].values[0]
            flags_df.loc[i,'KS_test'] = df_stats[df_stats.index==i]['KS_test'].values[0]    
            flags_df.loc[i,'KS_sum'] = df_stats[df_stats.index==i]['KS_train'].values[0] + df_stats[df_stats.index==i]['KS_test'].values[0]     


        for i in high_corred_flist_unique:
            high_corr_df.loc[high_corr_df.var1==i,'occurance_num_v1'] = flags_df[flags_df.index==i]['occurance_num'].values[0]
            high_corr_df.loc[high_corr_df.var2==i,'occurance_num_v2'] = flags_df[flags_df.index==i]['occurance_num'].values[0]

            high_corr_df.loc[high_corr_df.var1==i,'psi_maxval_v1'] = flags_df[flags_df.index==i]['psi_maxval'].values[0]
            high_corr_df.loc[high_corr_df.var2==i,'psi_maxval_v2'] = flags_df[flags_df.index==i]['psi_maxval'].values[0] 

            high_corr_df.loc[high_corr_df.var1==i,'gini_train_v1'] = flags_df[flags_df.index==i]['gini_train'].values[0]
            high_corr_df.loc[high_corr_df.var2==i,'gini_train_v2'] = flags_df[flags_df.index==i]['gini_train'].values[0] 

            high_corr_df.loc[high_corr_df.var1==i,'gini_test_v1'] = flags_df[flags_df.index==i]['gini_test'].values[0]
            high_corr_df.loc[high_corr_df.var2==i,'gini_test_v2'] = flags_df[flags_df.index==i]['gini_test'].values[0] 

            high_corr_df.loc[high_corr_df.var1==i,'gini_sum_v1'] = flags_df[flags_df.index==i]['gini_sum'].values[0]
            high_corr_df.loc[high_corr_df.var2==i,'gini_sum_v2'] = flags_df[flags_df.index==i]['gini_sum'].values[0]    

            high_corr_df.loc[high_corr_df.var1==i,'KS_train_v1'] = flags_df[flags_df.index==i]['KS_train'].values[0]
            high_corr_df.loc[high_corr_df.var2==i,'KS_train_v2'] = flags_df[flags_df.index==i]['KS_train'].values[0] 

            high_corr_df.loc[high_corr_df.var1==i,'KS_test_v1'] = flags_df[flags_df.index==i]['KS_test'].values[0]
            high_corr_df.loc[high_corr_df.var2==i,'KS_test_v2'] = flags_df[flags_df.index==i]['KS_test'].values[0]      

            high_corr_df.loc[high_corr_df.var1==i,'KS_sum_v1'] = flags_df[flags_df.index==i]['KS_sum'].values[0]
            high_corr_df.loc[high_corr_df.var2==i,'KS_sum_v2'] = flags_df[flags_df.index==i]['KS_sum'].values[0]    
        print(high_corred_flist_unique)
        print(high_corr_df.columns.tolist())
        high_corr_df['KS_diff'] = high_corr_df['KS_sum_v1']-high_corr_df['KS_sum_v2']
        high_corr_df['gini_diff'] = high_corr_df['gini_sum_v1']-high_corr_df['gini_sum_v2']
        high_corr_df['psi_diff'] = high_corr_df['psi_maxval_v1']-high_corr_df['psi_maxval_v2']
        high_corr_df['occurance_diff'] = high_corr_df['occurance_num_v1']-high_corr_df['occurance_num_v2']   

        high_corr_df['var1_score'] = high_corr_df['KS_diff'].apply(lambda x: 1 if x>0 else(-1 if x<0 else 0)) + high_corr_df['gini_diff'].apply(lambda x: 1 if x>0 else(-1 if x<0 else 0)) +\
        high_corr_df['psi_diff'].apply(lambda x: -1 if x>0 else(1 if x<0 else 0)) + high_corr_df['occurance_diff'].apply(lambda x: -1 if x>0 else(1 if x<0 else 0))

        high_corr_df['var2_score'] = (-1)*high_corr_df['KS_diff'].apply(lambda x: 1 if x>0 else(-1 if x<0 else 0)) + (-1)*high_corr_df['gini_diff'].apply(lambda x: 1 if x>0 else(-1 if x<0 else 0)) +\
        (-1)*high_corr_df['psi_diff'].apply(lambda x: -1 if x>0 else(1 if x<0 else 0)) + (-1)*high_corr_df['occurance_diff'].apply(lambda x: -1 if x>0 else(1 if x<0 else 0))

        high_corr_df['winner'] = (high_corr_df['var1_score'] - high_corr_df['var2_score']).apply(lambda x: 1 if x>0 else(2 if x<0 else 0))     

        # Формирование списка переменных к исключению
        to_drop_list = []
        for i in high_corr_df.index:
            if high_corr_df[high_corr_df.index==i].winner.values==2:
                to_drop_list.append(high_corr_df[high_corr_df.index==i].var1.values[0])
            else:
                to_drop_list.append(high_corr_df[high_corr_df.index==i].var2.values[0])
    #     list(set(to_drop_list))   
        final_flist = list(set(psi_norm)-set(to_drop_list))
    else:
        to_drop_list = []
        final_flist = psi_norm
    print()
    print()
    print()    
    print('Итог:')
    print(f'Longlist: {len(f_longlist)}')
    print(f'After missing filter: {len(norm_features)}')
    print(f'After psi filter: {len(psi_norm)}')
    print(f'After corr filter: {len(final_flist)}')
    print('')
    print(f'High correlated features: {len(high_corred_flist_unique)}')    
    print(f'High correlated features to be dropped: {len(set(to_drop_list))}')      
    

    
    return norm_features, psi_norm, final_flist, psi_df2, psi_df3, corr_df, df_stats, flags_df, high_corr_df, monitoring, scorecard, binning_process, feature_list_transformed, df_binned