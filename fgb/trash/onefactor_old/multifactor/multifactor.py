import pandas as pd
import numpy as np
from statsmodels.stats.outliers_influence import variance_inflation_factor
from tqdm import tqdm

class MultiFactorAnalysis:
    def __init__(self,df,stats_df, features_col, metrics_col, metrics_ascending=False):
        """ """
        self.df=df.copy()
        self.metrics_name=metrics_col
        self.features_col=features_col
        self.stats_df=stats_df[[features_col,metrics_col]].copy()
        self.stats_df = self.stats_df.sort_values(by=metrics_col,ascending=metrics_ascending)
        self.stats_df.reset_index(drop=True,inplace=True)

    def calc_stats(self
                   ,feature_list
                   ,check
                   ,max_window_length = np.nan
                   ,min_window_length = 1
                   ,metrics_range_percent = np.inf
                   ,verbose = True
                  ):
        result=[]
        droped_features = []
        feature_list = self.stats_df[self.stats_df[self.features_col].isin(feature_list)][self.features_col].to_list()
        for feature in tqdm(feature_list, disable=not verbose):
            #генерим группу фичей
  
            features_group=self.feature_list_maker(feature_name=feature
                                                   ,feature_list=feature_list
                                                   , max_window_length = max_window_length
                                                   , metrics_range_percent = metrics_range_percent
                                                   , droped_features = droped_features)
#             print(feature,features_group)
            df=MultifactorChecks._fill_na(self.df[features_group+[feature]],fillna_method = check['fillna_method'])
            stat = check['function'](df,feature,features_group,**check['params'])
            result.append(stat)
            if check['drop_condition'](stat): 
                droped_features.append(feature)
                
        result=pd.DataFrame(data={'feature_name':feature_list,check['name']:result})
        result.loc[result['feature_name'].isin(droped_features),'droped']=1
        
        return result
                
    def feature_list_maker(self
                     ,feature_name
                     ,feature_list
                     , max_window_length = np.nan
                     , metrics_range_percent = np.inf
                     , droped_features = []
                    ):
        if np.isnan(max_window_length):
            max_window_length=len(self.stats_df)
        
        stats_df=self.stats_df.copy()
        f=set(feature_list)-set(droped_features)
        stats_df=stats_df[stats_df[self.features_col].isin(f)].reset_index(drop=True)

        curr_ind = stats_df[stats_df[self.features_col]==feature_name].index[0]
        curr_metrics = stats_df.loc[curr_ind,self.metrics_name]
        stats_df['metrics_diff'] = 100*abs(stats_df[self.metrics_name]/curr_metrics-1)
        min_ind = stats_df[stats_df['metrics_diff']<metrics_range_percent].index.min()
        feature_group = stats_df.loc[min_ind:curr_ind-1,self.features_col].tail(max_window_length).values
        
        return list(feature_group)

class MultifactorChecks:    
    @staticmethod
    def _fill_na(df,fillna_method = 'ignore'):
        df=df.copy()
        if fillna_method=='ignore':
            return df
        if fillna_method=='median':
            return df.fillna(df.median())
        if fillna_method=='min':
            return df.fillna(df.min())
        if fillna_method=='max':
            return df.fillna(df.max()) 
        if fillna_method=='drop':
            return df.dropna()
        
    @staticmethod
    def _calc_corr(df,corr_method='spearman'):
        if df.shape[1]!=2:
            corr= np.nan
        if df.shape[1]<2:
            return 0        
        corr=df.corr(method=corr_method).values[0][1]
#         print(df.columns,corr)
        return corr 
    
    @staticmethod
    def calc_max_corr(df
                      ,feature
                      ,feature_with_list
                      ,corr_method='spearman'
                     ):
        max_corr=0
        max_corr_with=''
        for i in feature_with_list:
            corr = MultifactorChecks._calc_corr(df=df[[feature,i]],corr_method=corr_method)
            if abs(corr)>=abs(max_corr):
                max_corr=corr
                max_corr_with=i
        return max_corr
  
    @staticmethod
    def calc_vif(df,feature,feature_with_list):
        i = list(df[feature_with_list+[feature]].columns).index(feature)
        if len(feature_with_list)<1:
            return 0        
        return variance_inflation_factor(df[feature_with_list+[feature]].values,i)