from pandas.api.types import is_numeric_dtype
from scipy.stats import skew
from scipy.stats import kurtosis
import pandas as pd
import numpy as np

from tqdm import tqdm

class SimpleAnalysis:
    def __init__(self):
        '''
        Initialized simple analysis

        Parameters :
        -------------
        df : dict
            Summary datasets
        datasets_columns: dict
            Pairs pdataset:array of columns

        '''
        self.research = pd.DataFrame()
        
        
    def analysis(self, df, datasets_columns, research_functions, parameters, verbose=False):
        '''
        Used analysis function

        Parameters :
        -------------
        research_functions : dict
            Dict with analytics function

        '''
        count = 0
        for ds_name in datasets_columns:
            for f_name in datasets_columns[ds_name]:
                self.research.loc[count,'source'] = ds_name
                self.research.loc[count,'name'] = f_name
                self.research.loc[count,'error'] = 'OK'
                count += 1
        
        for rf_name in research_functions:
            if verbose:
                print('start', rf_name)
            for ds_name in datasets_columns:
                for f_name in tqdm(datasets_columns[ds_name], disable=(not verbose)):
                    try:
                        if rf_name not in parameters:
                            parameters[rf_name] = {}
                        self.research.loc[(self.research['source'] == ds_name) & (self.research['name'] == f_name), rf_name] = research_functions[rf_name](df[f_name], parameters[rf_name])
                    except Exception as e:
                        print('Error in = ', rf_name, ';', 'ds name = ', ds_name, ';', 'f_name = ', f_name, ';', 'error = ', e)
                        
    
    def full_analysis(self, df, datasets_columns, parameters = {}, verbose=False, with_slow=True):
        '''
        Used analysis function

        Parameters :
        -------------
        with_slow : bool
            Add very slow statistics

        Returns
        -------
        self.research
        '''
        if with_slow:
            self.analysis(df, datasets_columns, research_functions={
                 'dtype':SimpleAnalysisChecks.dtype_function,
                 'min':SimpleAnalysisChecks.min_func, 
                 'max':SimpleAnalysisChecks.max_func, 
                 'std': SimpleAnalysisChecks.std_func,
                 'mean': SimpleAnalysisChecks.mean_func, 
                 'median':SimpleAnalysisChecks.median_func, 
                 'n_unique':SimpleAnalysisChecks.n_unique,
                 'nan_percent':SimpleAnalysisChecks.nan_percent, 
                 'max_concentration':SimpleAnalysisChecks.max_concentration,
                 'max_concentration_value':SimpleAnalysisChecks.max_concentration_value,
                 'skew_func':SimpleAnalysisChecks.skew_func,
                 'kurtosis_func':SimpleAnalysisChecks.kurtosis_func,
                 'bootstraping_different_std_func':SimpleAnalysisChecks.bootstraping_different_std_func
            }, parameters=parameters, verbose=verbose)
        else:
            self.analysis(df, datasets_columns, research_functions={
                 'dtype':SimpleAnalysisChecks.dtype_function,
                 'min':SimpleAnalysisChecks.min_func, 
                 'max':SimpleAnalysisChecks.max_func, 
                 'std': SimpleAnalysisChecks.std_func,
                 'mean': SimpleAnalysisChecks.mean_func, 
                 'median':SimpleAnalysisChecks.median_func, 
                 'n_unique':SimpleAnalysisChecks.n_unique,
                 'nan_percent':SimpleAnalysisChecks.nan_percent, 
                 'max_concentration':SimpleAnalysisChecks.max_concentration,
                 'max_concentration_value':SimpleAnalysisChecks.max_concentration_value,
                 'skew_func':SimpleAnalysisChecks.skew_func,
                 'kurtosis_func':SimpleAnalysisChecks.kurtosis_func
            }, parameters=parameters, verbose=verbose)
    
class SimpleAnalysisChecks:
    '''
    https://www.ibm.com/docs/ru/spss-statistics/25.0.0?topic=descriptives-options
    https://www.codecamp.ru/blog/skewness-kurtosis-python/
    '''
    
    @staticmethod
    def dtype_function(ds, parameters):
        return str(ds.dtype)

    @staticmethod
    def min_func(ds, parameters):
        if is_numeric_dtype(ds):
            return np.min(ds)

    @staticmethod
    def max_func(ds, parameters):
        if is_numeric_dtype(ds):
            return np.max(ds)

    @staticmethod
    def sum_func(ds, parameters):
        '''
        Sum
        '''
        if is_numeric_dtype(ds):
            return np.sum(ds)
        
    @staticmethod
    def std_func(ds, parameters):
        '''
        Standart deviation(Стандартное отклонение)
        '''
        if is_numeric_dtype(ds):
            return np.std(ds)

    @staticmethod
    def mean_func(ds, parameters):
        '''
        Mean(Среднее)
        '''
        if is_numeric_dtype(ds):
            return np.mean(ds)
        
    @staticmethod
    def var_func(ds, parameters):
        '''
        Variance(Дисперсия)
        '''
        if is_numeric_dtype(ds):
            return np.var(ds)
        
    @staticmethod
    def range_func(ds, parameters):
        '''
        Range
        '''
        if is_numeric_dtype(ds):
            return np.abs(ds.max - ds.min)
        
    @staticmethod
    def bootstraping_different_std_func(ds, parameters):
        '''
        Bootstraping simple function
        
        Parameters ''
        '''
        
        if 'percent' not in parameters:
            percent = 5
        else:
            percent = parameters['percent']
        
        if is_numeric_dtype(ds):
            arr = []
            std_arr = []

            for count_in_sample in np.geomspace(1.0, len(ds), num=20):
                for i in range(100):
                    arr.append(ds.sample(round(count_in_sample), replace=True, random_state=777).mean())

                if np.std(arr) * 100 / np.mean(arr) < percent:
                    return count_in_sample
                arr = []

            return len(ds)
        
    @staticmethod
    def kurtosis_func(ds, parameters):
        '''
        Function calculated kurtosis
        
        Эксцесс — это мера того, является ли распределение тяжелым или легким хвостом по сравнению с нормальным распределением .

        Эксцесс нормального распределения равен 0.
        Если данное распределение имеет эксцесс меньше 0, говорят, что оно является игровым , что означает, что оно имеет тенденцию производить меньше и менее экстремальных выбросов, чем нормальное распределение.
        Если данное распределение имеет эксцесс больше 0, говорят, что оно лептокуртическое , что означает, что оно имеет тенденцию производить больше выбросов, чем нормальное распределение.
        '''
        if is_numeric_dtype(ds):
            return kurtosis(ds, bias = False)
        
    @staticmethod
    def skew_func(ds, parameters):
        '''
        Function calculated skew
        
        Асимметрия — это мера асимметрии распределения. Это значение может быть положительным или отрицательным.

        Отрицательная асимметрия указывает на то, что хвост находится в левой части распределения, которая простирается в сторону более отрицательных значений.
        Положительная асимметрия указывает на то, что хвост находится на правой стороне распределения, которая простирается в сторону более положительных значений.
        Нулевое значение указывает на то, что в распределении вообще нет асимметрии, что означает, что распределение совершенно симметрично.
        '''
        if is_numeric_dtype(ds):
            return skew(ds, bias = False)

    @staticmethod
    def median_func(ds, parameters):
        if is_numeric_dtype(ds):
            return np.median(ds)

    @staticmethod
    def n_unique(ds, parameters):
        return ds.nunique(dropna=False)

    @staticmethod
    def nan_percent(ds, parameters):
        return ds.isna().mean()*100

    @staticmethod
    def max_concentration(ds, parameters):
        return ds.value_counts().head(1).values[0]/len(ds)

    @staticmethod
    def max_concentration_value(ds, parameters):
        return ds.value_counts().head(1).index[0]
    
    @staticmethod
    def bootstraping_different_level(ds, parameters):
        '''
            Temp(not complete)
        '''
        if is_numeric_dtype(ds):

            if 'array_of_counts' not in parameters:
                array_of_counts = np.geomspace(2.0, len(ds), num=10)
            else:
                array_of_counts = parameters['array_of_counts']

            if 'counts_subsample_in_one_test' not in parameters:
                counts_subsample_in_one_test = 10
            else:
                counts_subsample_in_one_test = parameters['counts_subsample_in_one_test']

    #         if 'p_value' not in parameters:
    #             p_value = 0.01
    #         else:
    #             p_value = parameters['p_value']

            arr = []
            p_value_arr = []


            for count_in_sample in tqdm(array_of_counts):
                flag = True
                for i in range(counts_subsample_in_one_test):
                    answer = ds.sample(round(count_in_sample), replace=True, random_state=777 + i)

                    for prev_i in range(len(arr)):
                        statistics, p_value_temp = ttest_ind(arr[prev_i], answer)
                        p_value_arr.append(p_value_temp)

                    arr.append(answer)
                print(len(p_value_arr))
                print(st.t.interval(alpha=0.95, df=len(p_value_arr)-1, loc=np.mean(p_value_arr), scale=st.sem(p_value_arr)) )

                arr = []
                p_value_arr = []

            return len(ds)