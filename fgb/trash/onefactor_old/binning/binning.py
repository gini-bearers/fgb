import os
import pickle
from optbinning import OptimalBinning
from tqdm import tqdm
import pandas as pd
import warnings

class BinningFramework:
    def __init__(self, df, queries, train_query, save_field, features_to_binning, categorial_features, target, log_path='./'):
        
        self.df = df
        self.queries = queries
        self.train_query = train_query
        self.features_to_binning = features_to_binning
        self.categorial_features = categorial_features
        self.target = target
        self.save_field = save_field
        
        self.change_feature_name = []
        self.optb_log = {} 
        self.log_path = log_path 
        
        self.df_binning = pd.DataFrame()
        
    def transform(self, binning_params = {'solver':'cp'}, verbose=False):
        df_copy = self.df[self.features_to_binning + self.save_field + [self.target]].copy()
        df_copy.reset_index(inplace=True)

        # массив для обучения

        train = df_copy[df_copy.eval(self.queries[self.train_query])]

        binning_dict = {}

        for field in tqdm(self.features_to_binning, disable=(not verbose)):

            binning_params_auto = self.__change_binning_params(field, binning_params)       
            optb = OptimalBinning(**binning_params_auto)
            optb.fit(train[field], train[self.target])
            binning_dict[field + '_mono_binned'] = optb.transform(df_copy[field], metric_missing='empirical')

            self.change_feature_name.append(field + '_mono_binned')

            self.optb_log[field] = optb

        self.df_binning = pd.DataFrame(binning_dict)
        for i in self.save_field:
            self.df_binning[i] = list(df_copy[i])

        self.save_log()

    def __change_binning_params(self, field, binning_params):
        binning_params['name'] = field
        if field in self.categorial_features:
            binning_params['dtype'] = 'categorical'
            binning_params['monotonic_trend'] = 'auto'
        else:
            binning_params['dtype'] = 'numerical'   
            binning_params['monotonic_trend'] = 'auto_asc_desc'         
        return binning_params

    def save_log(self):
        os.makedirs(os.path.dirname(self.log_path+'/'), exist_ok=True)
        os.makedirs(os.path.dirname(self.log_path+'/'+'optb_log/'), exist_ok=True)
        with open(f'{self.log_path}/optb_log/optb_log.pkl', 'wb') as f:
            pickle.dump(self.optb_log, f)

    def load_log(self, path):
        with open(path, 'rb') as f:
            self.optb_log = pickle.load(f)

    def transform_new_sample(self, df: pd.DataFrame, ids:list) -> pd.DataFrame:
        '''
        Бинаризовать выборку, используя обученные бинаризаторы
        :param df: - датасет
        :param ids: - список id, который надо оставить в датасете
        :return: - бинаризованный датасет
        '''
        result = df[ids].copy()
        for f in df.columns:
            if f in self.optb_log.keys():
                optb = self.optb_log[f]
                result[f] = optb.transform(df[f])
        return result
        