from sklearn.linear_model import LinearRegression
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects
import matplotlib
import seaborn as sns


class calibration():
    def __init__(self
                 ,clb_set 
                 ,effects_set 
                 ,target_name
                 ,score_name 
                 ,score_bins_name 
                 ,grade_groups
                ): 
        '''
        calibration formula : 1/(1+math.exp(b0 + b1*(x-score_median)))
                            or 1/(1+exp(a*scoreb))
                            where a = b1,  b = b0 - b1*score_median
        
        Parameters :
        -------------
        clb_set : pd.Dataframe
            Dataset to calculate calib
            
        effects_set : pd.Dataframe
            Dataset to calculate effects
            
        target_name : string
            Name of calibration target            
            
        score_name : string
            Name of score for calibration
            
        score_bins_name : string
            Name of score bins for calibration

        '''
        self.clb_set=clb_set.copy()    
        self.score_name = score_name
        self.score_bins_name = score_bins_name
        self.target_name = target_name
        self.effects_set=effects_set.copy()
        self.results = pd.DataFrame() # результаиы калибровок
        self.gr_score={} # табличка по которой стороилась калибровка
        self.cut_off=[] # cut_off в заданной итерации
        self.grade_groups = grade_groups
    

    def set_score_bins(self,new_bins_name):
        self.score_bins_name = new_bins_name
    
    def correction_ct(self,x, CT, SDR): # Баесовская корректировка
        return (x * (CT / SDR)) / ( (1-x) * ((1 - CT) / (1 - SDR)) + x * (CT / SDR))
    
    def get_pd(self,x,b0,b1,score_median): #расчет PD
        return 1/(1+math.exp(b0 + b1*(x-score_median)))
            
    def get_grade(self,x):
        if x < 0 : raise Exception('invalid PD')
        # добавить грейды
        else : raise Exception('invalid PD')

    def get_grade_group(self, grade_groups, grade):
        for group in grade_groups:
            if grade >= grade_groups[group][0] and grade <= grade_groups[group][1]:
                return group
        return grade

    def calc_parity(self,pd,target,cut_off):
        real_bads_total = target.sum()
        real_bads_cut_off= target[pd<cut_off].sum()
        
        model_bads_total = pd.sum()
        model_bads_cut_off= pd[pd<cut_off].sum()
        
        return {'parity_total': real_bads_total / model_bads_total
                ,'parity_cut_off' : real_bads_cut_off / model_bads_cut_off
               } 
        
    def calc_results(self, ind, grade_groups):
        
        b0 = self.results.loc[ind,'b0']
        b1 = self.results.loc[ind,'b1']
        score_median = self.results.loc[ind,'score_median']
        
        #рассчет PD и грейдов       
        self.clb_set[f'pd_clb{ind}'] = self.clb_set[self.score_name].apply(lambda x: self.get_pd(x,b0,b1,score_median))
        self.clb_set[f'grade_clb{ind}'] = self.clb_set[f'pd_clb{ind}'].apply(lambda x: self.get_grade(x))
        self.clb_set[f'grade_group_clb{ind}'] = self.clb_set[f'grade_clb{ind}'].apply(lambda x: self.get_grade_group(grade_groups,x))
        
        self.effects_set[f'pd_clb{ind}'] = self.effects_set[self.score_name].apply(lambda x: self.get_pd(x,b0,b1,score_median))
        self.effects_set[f'grade_clb{ind}'] = self.effects_set[f'pd_clb{ind}'].apply(lambda x: self.get_grade(x))
        self.effects_set[f'grade_group_clb{ind}'] = self.effects_set[f'grade_clb{ind}'].apply(lambda x: self.get_grade_group(grade_groups,x))
        
        # parity
        parity = self.calc_parity( self.clb_set[f'pd_clb{ind}'],self.clb_set[self.target_name], self.cut_off[ind] )
        self.results.loc[ind,'parity_total'] = parity['parity_total']
        self.results.loc[ind,'parity_cut_off'] = parity['parity_cut_off']
        
        uniq_grades = sorted(self.clb_set[f'grade_group_clb{ind}'].unique())
        # parity by grade_group        
        for i in uniq_grades :
            parity = self.calc_parity( self.clb_set.loc[self.clb_set[f'grade_group_clb{ind}']==i, f'pd_clb{ind}']
                                      ,self.clb_set.loc[self.clb_set[f'grade_group_clb{ind}']==i, self.target_name]
                                      , self.cut_off[ind] )
            self.results.loc[ind,f'{i}_parity'] = parity['parity_total']
            
        # mix by grade
        for i in uniq_grades :
            self.results.loc[ind,f'{i}_clb_mix'] = np.sum(self.clb_set[f'grade_group_clb{ind}']==i)/len(self.clb_set[f'grade_group_clb{ind}'])  
            
        uniq_grades = sorted(self.effects_set[f'grade_group_clb{ind}'].unique())
        for i in uniq_grades :
            self.results.loc[ind,f'{i}_eff_mix'] = np.sum(self.effects_set[f'grade_group_clb{ind}']==i)/len(self.effects_set[f'grade_group_clb{ind}'])
        
        
        self.results.loc[ind,f'approve_rate'] =  np.sum(self.effects_set[f'pd_clb{ind}']<self.cut_off[ind])/len(self.effects_set)
    
    def calc_calib(self
                   ,parity
                   ,cut_off_dr = np.inf
                  ):
        '''
        Parameters :
        -------------
        parity : requested parity to calculate central tendention of default rate CT = SDR / parity.
                Where SDR - mean defaul rate of using dataset
        
        cut_off_dr : bins with mean DR hier then cut_off_dr will be excluded from calculation           
        '''
        self.cut_off.append(cut_off_dr)
        gr_score=self.clb_set.groupby(by= self.score_bins_name ).agg({self.score_name:['min','max']
                                                                      ,self.target_name:['count','sum','mean']})
        gr_score.reset_index(drop=False, inplace=True)
        gr_score.columns = ['score_bin'
                            ,'min_score','max_score'
                            ,'cnt','bad_sum','dr']
        gr_score['real_grade'] = gr_score.dr.apply(lambda x: self.get_grade(x))
        gr_score['score_mean'] = self.clb_set.groupby(by=self.score_bins_name)[self.score_name].mean().values
        
        for i in gr_score.dr:
            if i== 1 or i== 0:
                raise Exception('Clear bins are not supported')
        
        SDR = self.clb_set[self.target_name].mean() 
        score_median = np.median(self.clb_set[self.score_name])
        

        CT = SDR/parity #ожидаемая центральная тенденция

        gr_score['dr_scaled'] = gr_score.dr.apply(lambda x : self.correction_ct(x, CT,SDR)) # корректировка Байеса prity
        gr_score['dr_y_scaled'] =  np.log ((1-gr_score.dr_scaled)/(gr_score.dr_scaled)) 
        gr_score['x'] = gr_score.score_mean - score_median
        
        # строим МНК по точкам
        lr_corr = LinearRegression()
        
        score_cut_off = gr_score[gr_score.dr_scaled<cut_off_dr]
        score_cut_off = score_cut_off[score_cut_off.dr_scaled == score_cut_off.dr_scaled.max()]['score_mean'].values[0]
        gr_score['score_mean_cut_off'] = score_cut_off
        lr_corr.fit(gr_score[gr_score.dr_scaled<cut_off_dr].x.values.reshape(-1,1)
                    , gr_score[gr_score.dr_scaled<cut_off_dr].dr_y_scaled.values)
        b0, b1 = lr_corr.intercept_, lr_corr.coef_[0]
        #gr_score['predict'] = lr_corr.predict(gr_score.x.values.reshape(-1,1))
        gr_score['bin_pd_clb'] = gr_score['score_mean'].apply(lambda x : self.get_pd(x,b0,b1,score_median) )
        gr_score['bin_parity'] = gr_score['dr']/gr_score['bin_pd_clb']
        gr_score['bin_parity_scaled'] = gr_score['dr_scaled']/gr_score['bin_pd_clb']

        gr_score.sort_values(by='score_mean',inplace=True,ignore_index=True)
        gr_score['parity_issued'] = parity
        
        if len(self.results)>0:
            n = self.results.index[-1] + 1
        else : n = 0 
            
        self.gr_score[n]=gr_score
        
        #сохраняем коеффициенты в результат        
        self.results.loc[n,f'bins_name'] = self.score_bins_name
        self.results.loc[n,'base_n'] = None
        self.results.loc[n,'parity_issued'] = parity
        self.results.loc[n,'cut_off'] = cut_off_dr
        self.results.loc[n,'b0'] = b0
        self.results.loc[n,'b1'] = b1
        self.results.loc[n,'score_median'] = score_median
        self.results.loc[n,'a'] = b1
        self.results.loc[n,'b'] = b0 - b1*score_median        

        self.calc_results(n, self.grade_groups)
        
        return gr_score
    
    def transform_clb(self,clb_ind,cut_off_dr ,stretch_k=1, shift=0, median_k=1):
        self.cut_off.append(cut_off_dr)
        
        b0 = self.results.loc[clb_ind,'b0'] + shift
        b1 = self.results.loc[clb_ind,'b1'] * stretch_k
        score_median = self.results.loc[clb_ind,'score_median'] * median_k
        
        if len(self.results)>0:
            n = self.results.index[-1] + 1
        else : n = 0 
        
        self.results.loc[n,f'bins_name'] = None
        self.results.loc[n,'base_n'] = clb_ind
        self.results.loc[n,'parity_issued'] = None
        self.results.loc[n,'cut_off'] = cut_off_dr
        self.results.loc[n,'b0'] = b0
        self.results.loc[n,'b1'] = b1
        self.results.loc[n,'score_median'] = score_median
        self.results.loc[n,'a'] = b1
        self.results.loc[n,'b'] = b0 - b1*score_median  
        
        self.calc_results(n, self.grade_groups)
        
    def merge_bins(self
                   ,bins_to_merge
                   ,gr_score
                   ,new_bins_save_name
                  ):

        to_merge = set()
        # проверка на монотонность объединения
        for group in bins_to_merge:
            for i,ind in enumerate(sorted(group)[:-1]):
                
                if abs(group[i]-group[i+1])>1:
                    raise Exception('not monotonic merge')
        
        for group in bins_to_merge:
            for i,ind in enumerate(sorted(group)):                    
                if ind in to_merge:
                    raise Exception('duplicated bin')
                else:
                    to_merge.add(ind)
        mins=set()
        maxes=set()           
        for i in gr_score.index:

            in_group_index=-1

            for n,group in enumerate(bins_to_merge):
                if i in group:
                    in_group_index=n
                    break

            if in_group_index>=0:    
                print(f'TRUE {i} in group {in_group_index}')
                mins.add(gr_score.loc[np.min(bins_to_merge[in_group_index]),'min_score'])
                maxes.add(gr_score.loc[np.max(bins_to_merge[in_group_index]),'max_score'])
            else:
                print(f'FALSE {i} in group {in_group_index}')
                mins.add(gr_score.loc[i,'min_score'])
                maxes.add(gr_score.loc[i,'max_score'])
            
            self.clb_set[new_bins_save_name] = pd.cut(self.clb_set[self.score_name]
                                                        ,bins=sorted(mins)+[np.inf]
                                                        ,right=False
                                                        ,include_lowest =True
                                                     )
            

   
    def del_clb(self,clb_ind):
        self.clb_set.drop(columns = [f'pd_clb{clb_ind}',f'grade_clb{clb_ind}',f'grade_group_clb{clb_ind}'],inplace=True)
        self.effects_set.drop(columns = [f'pd_clb{clb_ind}',f'grade_clb{clb_ind}',f'grade_group_clb{clb_ind}'],inplace=True)
        self.results.drop(index=clb_ind,inplace=True,axis=1)
        try:
            del self.gr_score[clb_ind]
        except:
            1==1
            
    def plot_clb_building(self,gr_score): # график построение калибровки 
        fig, (ax1,ax2) = plt.subplots(ncols = 2, figsize=(16,7))

        sns.lineplot(gr_score.score_mean,gr_score['dr']
                     ,color='g', marker='o', label=f'DR'
                     ,zorder=-5
                     ,ax=ax1)
        sns.lineplot(gr_score.score_mean,gr_score['dr_scaled']
                     ,color='y', marker='o', label=f'DR_scaled'
                     ,zorder=-5
                     ,ax=ax1)        
        
        sns.lineplot(gr_score.score_mean,gr_score['bin_pd_clb']
                     ,color='r', marker='o', label=f'PD_CLB'
                     ,zorder=-5
                     ,ax=ax1)

        ax1.vlines(gr_score.score_mean_cut_off.max(),ymin=0
                   ,ymax=np.max([gr_score['dr_scaled'].max(),gr_score['dr'].max(),gr_score['bin_pd_clb'].max()])
                   , color = 'r',label='cut_off')
        
        ax1.tick_params(axis='y',labelsize ='x-large')
        ax1.tick_params(axis='x', rotation=45)  
        ax1.set_ylabel('Default Rate',fontsize = 'x-large')
        #ax1.set_xticklabels(gr_score.score_mean.round(2))
        # loc = plticker.MultipleLocator(base=1.0) # this locator puts ticks at regular intervals
        # ax1.xaxis.set_major_locator(loc)
        ax1.legend(loc='upper left',fontsize = 'large',framealpha =1)
        ax1.grid(color='gray', zorder=-5)

        sns.barplot(x=gr_score['score_mean'].round(3), y=gr_score['bin_parity']
                   , color='cornflowerblue', label='bin_parity'
                   ,zorder=1
                   ,ax=ax2)
        plt.bar_label(ax2.containers[0],
                      labels=gr_score.index, 
                      color='black', 
                      zorder=110,
                      label_type='edge',
                      fontsize = 'x-large',
                     rotation=45
                     )
        ax2.set_ylabel('Bin Parity',fontsize = 'x-large')
        ax2.grid(color='gray', zorder=-5)
        ax2.legend(loc='upper left',fontsize = 'x-large',framealpha =1)
        ax2.set_xticklabels(gr_score.score_mean.round(2))
        plt.xticks(rotation = 45)
        ax2.set_ylim(0,gr_score['bin_parity'].max()+0.2*gr_score['bin_parity'].max())
        
        
        ax2.plot(gr_score['parity_issued'],color='r',linestyle='dashed',linewidth=1.5)
        ax2.vlines(gr_score[gr_score.score_mean==gr_score.score_mean_cut_off.max()].index[0]
                   ,ymin=0
                   ,ymax=gr_score['bin_parity'].max()+0.2*gr_score['bin_parity'].max()
                   , color = 'r'
                   ,label='cut_off')
        
        fig.suptitle('Calib set stats',fontsize = 'xx-large')
        plt.show() 
                   
    def plot_results(self,ind):
        fig, ax = plt.subplots(ncols = 3,nrows=len(ind)+1, figsize=(32,11*len(ind)))
        
        sns.barplot(['CLB '+str(i) for i in ind],self.results.loc[ind,'parity_cut_off'],ax=ax[0,0]
                    ,label=f'parity_cut_off')
        ax[0,0].set_title(f'PARITY TOTALS')
        ax[0,0].bar_label(ax[0,0].containers[0],
                      labels=[f'{self.results.loc[i,"parity_cut_off"].round(2)}' for i in ind], 
                      color='black', 
                      zorder=110,
                      label_type='edge',
                      fontsize = 'large')


#         ax[i+1,0].set_ylabel('Bin Parity',fontsize = 'large')
#         ax[i+1,0].grid(color='gray', zorder=-5)
#         ax[i+1,0].legend(loc='upper right',fontsize = 'large',framealpha =1)
#         ax[i+1,0].set_ylim(0,gr_set_clb['parity_cut_off'].max()+0.2*gr_set_clb['parity_cut_off'].max())
        for i in ind:
            sns.kdeplot(self.clb_set[f'pd_clb{i}'], log_scale=True, ax=ax[0,1], label=f'pd_clb{i}')
            sns.kdeplot(self.effects_set[f'pd_clb{i}'], log_scale=True, ax=ax[0,2], label=f'pd_clb{i}')
        
        ax[0,1].set_xlabel('PD_clb',fontsize = 'large')
        ax[0,1].grid(color='gray')
        ax[0,1].legend(loc='upper left',fontsize = 'large',framealpha =1)
        ax[0,1].set_title('CLB SET PD DISTRIBUTON')
        
        ax[0,2].set_xlabel('PD_clb',fontsize = 'large')
        ax[0,2].grid(color='gray')
        ax[0,2].legend(loc='upper left',fontsize = 'large',framealpha =1)
        ax[0,2].set_title('EFFECTS SET PD DISTRIBUTON')
        
        for plt_i,i in enumerate(ind):
            gr_set_clb=self.clb_set.groupby(by=f'grade_group_clb{i}').agg({self.score_name:'count',self.target_name:'sum',f'pd_clb{i}':'sum'})
            gr_set_clb.columns=['cnt','bad_cnt','pd_sum']
            gr_set_clb['share'] = gr_set_clb['cnt']/gr_set_clb['cnt'].sum()
            gr_set_clb['parity_cut_off'] = gr_set_clb.bad_cnt/gr_set_clb.pd_sum
            
            gr_set_clb.reset_index(inplace=True)
            
            parity = self.results.loc[i,'parity_cut_off']
            sns.barplot(gr_set_clb[f'grade_group_clb{i}'], gr_set_clb[f'parity_cut_off'],ax=ax[plt_i+1,0]
                        ,label=f'grade_group_clb{i} parity' ,color='cornflowerblue')
            ax[plt_i+1,0].set_title(f'GRADE PARITY {i} CLB SET')
            ax[plt_i+1,0].bar_label(ax[plt_i+1,0].containers[0],
                          labels=gr_set_clb['parity_cut_off'].round(2), 
                          color='black', 
                          zorder=110,
                          label_type='edge',
                          fontsize = 'large')
            ax[plt_i+1,0].plot([parity for i in gr_set_clb['parity_cut_off']]
                           ,color='r',linestyle='dashed',linewidth=1.5,label=f'parity cut-off= {round(parity,2)}')
            
            ax[plt_i+1,0].set_ylabel('Bin Parity',fontsize = 'large')
            ax[plt_i+1,0].grid(color='gray', zorder=-5)
            ax[plt_i+1,0].legend(loc='upper right',fontsize = 'large',framealpha =1)
            ax[plt_i+1,0].set_ylim(0,gr_set_clb['parity_cut_off'].max()+0.2*gr_set_clb['parity_cut_off'].max())
            
            
            
            
            sns.barplot(gr_set_clb[f'grade_group_clb{i}'],gr_set_clb['cnt'],ax=ax[plt_i+1,1]
                        ,color='cornflowerblue', label=f'grade_group_clb{i}')
            ax[plt_i+1,1].bar_label(ax[plt_i+1,1].containers[0]
                                ,labels=[str(i)+'%' for i in np.round(100*gr_set_clb['share'],1)]
                                ,color='black'
                                ,zorder=110
                                ,label_type='edge'
                                ,fontsize = 'large')
            
            ax[plt_i+1,1].set_xlabel(f'grade {i}')
            ax[plt_i+1,1].grid(color='gray')
            ax[plt_i+1,1].legend(loc='upper right',fontsize = 'large',framealpha =1)
            ax[plt_i+1,1].set_title(f'GRADE DISTRIBUTION {i} CLB SET')
            
            
            gr_set_eff=self.effects_set.groupby(by=f'grade_group_clb{i}').agg({self.score_name:'count',f'pd_clb{i}':'sum'})
            gr_set_eff.columns=['cnt','pd_sum']
            gr_set_eff['share'] = gr_set_eff['cnt']/gr_set_eff['cnt'].sum()
            gr_set_eff.reset_index(inplace=True)
            sns.barplot(gr_set_eff[f'grade_group_clb{i}'],gr_set_eff['cnt'],ax=ax[plt_i+1,2]
                        ,color='cornflowerblue'
                        , label=f'grade_group_clb{i}, AR = {self.results.loc[i,"approve_rate"].round(2)}')
            ax[plt_i+1,2].bar_label(ax[plt_i+1,2].containers[0]
                                ,labels=[str(i)+'%' for i in np.round(100*gr_set_eff['share'],1)]
                                ,color='black'
                                ,zorder=110
                                ,label_type='edge'
                                ,fontsize = 'large')
            ax[plt_i+1,2].set_xlabel(f'grade {i}')
            ax[plt_i+1,2].grid(color='gray')
            ax[plt_i+1,2].legend(loc='upper right',fontsize = 'large',framealpha =1)
            ax[plt_i+1,2].set_title(f'GRADE DISTRIBUTION {i} EFF SET')
            
    def buld_dataset(self,df,score_name,ind):
        df=df.copy()
        for n in ind:
            b0 = self.results.loc[n,'b0']
            b1 = self.results.loc[n,'b1']
            score_median = self.results.loc[n,'score_median']
            df[f'pd_clb{n}'] = df[score_name].apply(lambda x: self.get_pd(x,b0,b1,score_median))
            df[f'grade_clb{n}'] = df[f'pd_clb{n}'].apply(lambda x: self.get_grade(x))
            df[f'grade_group_clb{n}'] = df[f'pd_clb{n}'].apply(lambda x: self.get_grade_group(x))
        return df