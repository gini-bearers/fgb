import pandas as pd
from scipy import stats
from IPython.display import clear_output
import json
import time
import gzip
import optuna
import os
import pickle

import copy

class FeatureSelectionByOptuna():
    def __init__(self
                 ,sampler
                 ,sampler_keys = {}
                 ,seed=777 
                 ,direction = 'maximize'
                 ,feature_list=[] 
                 ,fixed_features = []
                 ,log_path='./'  
                ):
        '''
        Class for feature selection by optuna
        
        Parameters :
        -------------
        sampler : optuna.samplers._tpe.sampler
            One of optuna samplers(example TPESampler, RandomSampler, etc.)
        
        sampler_keys : dict
            dict of sampler parametrs
        
        seed : int
            Random seed 
        
        direction : 'maximize' or 'minimize'
            Direction of optimization
        
        feature_list : list
            List of feature 
        
        fixed_features : list
            List of feature who don't delete
        
        log_path : str
            Path for log
        
        '''

        self.sampler = sampler(seed=seed, **sampler_keys)  # Make the sampler behave in a deterministic way.
        self.study = optuna.create_study(direction = direction, sampler=self.sampler)
        self.seed = seed
        self.feature_list = feature_list
        self.fixed_features = fixed_features
        self.log_path = log_path
        
        self.model_static_params = {}
        self.model_search_params = {}
        self.func_params = {}
        self.metric_func_params = {}
        self.droped_features=set()
        self.information_from_scores = []
        self.next_number_iteration = 0
        self.run_time_str = 0
        
    def set_fit_and_score_function(self,func):
        '''
        Set function to fit model
        
        Parameters :
        -------------
        func : Function
            Set function to fit model. Parameters function maybe custom and fill on function set_objective_params()
            
        '''
        self.func=func
    
    def set_metrics_function(self, func):
        '''
        Set metric fuction
        
        Parameters :
        -------------
        func : Function
            Set calculate metric by array of scores. Parameters function maybe custom and fill on function set_objective_params()
            
        '''
        self.metrics=func
    
    def set_objective_params(self
                             ,model_static_params
                             ,model_search_params
                             ,func_params
                             ):
        '''
        Fuction to fill parametrs custom function
        
        Parameters :
        -------------            
        model_static_params : dict
            Params of model to fit function(catboost, etc.)
            
        model_search_params : dict
            Params of optuna

        func_params : dict
            Params of func_params
        '''
        
        for i in model_search_params.keys():
            self.model_search_params[i]=model_search_params[i]
        
        for i in model_static_params.keys():
            self.model_static_params[i]=model_static_params[i]
        
        for i in func_params.keys():
            self.func_params[i]=func_params[i]
    
    def study_features_info(self):
        '''
        Method for get info by optuna
        
        '''
        
        st = self.study.trials_dataframe()
        st= st[st.state == 'COMPLETE']
        res=pd.DataFrame()
        for i,n in enumerate(self.feature_list):
            res.loc[i,'variable'] = n
            res.loc[i,'cnt'] = st['params_'+n].sum()
            res.loc[i,'cnt%'] = st['params_'+n].sum()/len(st)
            res.loc[i,'mean_metrics_with'] = st[st['params_'+n]==1].value.mean()
            res.loc[i,'mean_metrics_wo'] = st[st['params_'+n]==0].value.mean()
            res.loc[i,'mean_metrics_diff'] = res.loc[i,'mean_metrics_with'] - res.loc[i,'mean_metrics_wo']
            res.loc[i,'median_metrics_with'] = st[st['params_'+n]==1].value.median()
            res.loc[i,'median_metrics_wo'] = st[st['params_'+n]==0].value.median()
            res.loc[i,'median_metrics_diff'] = res.loc[i,'median_metrics_with'] - res.loc[i,'median_metrics_wo'] 

            res.loc[i,'ttest_greater_p']= stats.ttest_ind(st.loc[st['params_'+n]==0,'value']
                                                 ,st.loc[st['params_'+n]==1,'value']
                                                 ,alternative = 'greater' )[1] 

            res.loc[i,'ttest_less_p']= stats.ttest_ind(st.loc[st['params_'+n]==0,'value']
                                                 ,st.loc[st['params_'+n]==1,'value']
                                                 ,alternative = 'less' )[1]

            res.loc[i,'ttest_2sided_p']= stats.ttest_ind(st.loc[st['params_'+n]==0,'value']
                                                 ,st.loc[st['params_'+n]==1,'value']
                                                 ,alternative = 'two-sided' )[1] 
            res.sort_values(by='ttest_less_p',ignore_index=True,inplace=True)
            

        return res        
    
    def iteration_info(self):
        '''
        Method for get optuna iteration metrics(in this print have custom metrics)
        
        '''
        
        return self.study.trials_dataframe().merge(pd.DataFrame(self.information_from_scores), on='number')        
        
    def objective(self, trial):            
        features_selection_final={}
        features_selection_final_arr=[]

        for i in self.feature_list:
            if i in self.droped_features:
                features_selection_final[i] = trial.suggest_int(i, 0, 0, 1)
            elif i in self.fixed_features:
                features_selection_final[i] = 1
            else :
                features_selection_final[i] = trial.suggest_int(i, 0, 1, 1) 

            if features_selection_final[i] == 1:
                features_selection_final_arr.append(i)

        params = {
            'feature_list':features_selection_final_arr
        }
        model_params=self.model_static_params

        for i in self.model_search_params.keys():

            if self.model_search_params[i][0] == 'int': #type
                 model_params[i]=trial.suggest_int(i #name
                                             ,self.model_search_params[i][1]#min
                                             ,self.model_search_params[i][2]#max
                                             ,self.model_search_params[i][3]#step
                                            )
            elif self.model_search_params[i][0] == 'float': #type
                 model_params[i]=trial.suggest_float(i #name
                                             ,self.model_search_params[i][1] #min
                                             ,self.model_search_params[i][2] #max  
                                            # ,self.model_search_params[i][3] #step 
                                            )
            # {i:(name str, values list)}
            elif self.model_search_params[i][0] == 'categorical':
                model_params[i]=trial.suggest_categorical(i #name
                                             ,self.model_search_params[i][1] #values 
                                            )
            else:
                raise Exception(f'Wrong param type: {self.model_search_params[i]}. It should be "int", "float" or "categorical"')
                
        params['model_params']=model_params
        self.score=self.func(copy.deepcopy(params), **self.func_params)
        
        self.score['number'] = self.next_number_iteration
        self.next_number_iteration += 1
        self.score['metrics'] = self.metrics(self.score)
        
        self.information_from_scores.append(self.score)

        return self.score['metrics'] 
    
    def save_optuna_state(self, epoch, log_path, run_time_str):
        '''
        Fuction save class optuna
        
        Parameters :
        -------------            
        epoch : number
            Number of epoch
            
        log_path : str
            Path to log

        run_time_str : str
            Path to dict
        '''
        
        os.makedirs(os.path.dirname(self.log_path+'/'), exist_ok=True)
        os.makedirs(os.path.dirname(self.log_path+'/'+self.run_time_str+'/'), exist_ok=True)
        
        # save
        # study_features_info
        self.fi.to_csv(f'{log_path}/{run_time_str}/optuna_study_features_{epoch}.csv')
        # study
        with open(f'{log_path}/{run_time_str}/optuna_exp_save_{epoch}.pkl', 'wb') as handle:
            pickle.dump(self.study, handle)
        # information_from_scores
        with gzip.open(f'{log_path}/{run_time_str}/optuna_information_from_scores_{epoch}.gz', 'wb') as handle:
            pickle.dump(self.information_from_scores, handle)
        # droped_features
        with open(f'{log_path}/{run_time_str}/optuna_droped_features_{epoch}.pkl', 'wb') as handle:
            pickle.dump(self.droped_features, handle)
        # base params
        with open(f'{log_path}/{run_time_str}/optuna_extra_params.pkl', 'wb') as handle:
            params = {'model_static_params':self.model_static_params,
                      'feature_list':self.feature_list}
            pickle.dump(params, handle)
            
    def load_optuna_state(self, directory, epoch, format='gz'):
        '''
        Fuction load class optuna
        
        Parameters :
        -------------
        directory : str
            Path to dict
            
        epoch : number
            number of epoch
        '''
        
        self.fi = pd.read_csv(f'{directory}/optuna_study_features_{epoch}.csv')
        with open(f'{directory}/optuna_exp_save_{epoch}.pkl', 'rb') as handle:
            self.study = pickle.load(handle)
#         with open(f'{directory}/optuna_information_from_scores_{epoch}.pkl', 'rb') as handle:
#             self.information_from_scores = pickle.load(handle)
        with open(f'{directory}/optuna_droped_features_{epoch}.pkl', 'rb') as handle:
            self.droped_features = pickle.load(handle)
        
        if format == 'gz':
            with gzip.open(f'{directory}/optuna_information_from_scores_{epoch}.gz', 'rb') as handle:
                self.information_from_scores = pickle.load(handle)
        else:
            with open(f'{directory}/optuna_information_from_scores_{epoch}.pkl', 'rb') as handle:
                self.information_from_scores = pickle.load(handle)
        if 'optuna_extra_params.pkl' in os.listdir(directory):
            with open(f'{directory}/optuna_extra_params.pkl', 'rb') as handle:
                params = pickle.load(handle)
                self.model_static_params = params['model_static_params']
                self.feature_list = params['feature_list']
        else:
            print('No base params')
            self.model_static_params = {}
            self.feature_list = []
        
    def opt_study(self
                  ,period_length
                  ,n_periods
                  ,p_value_tres=-1
                 ):
        '''
        Method for study optuna
        
        Parameters :
        -------------
        period_length : int
            Count of iteration optuna in periods
        
        n_periods : int
            Count of periods logs model 
        
        p_value_tres : float
            Level of p-value, if feature ttest_less > p_value, then drop. 
            Example: Set 0.95, features Fe1 get p-value > 0.95, it's mean Fe1 combination with Fe1 less then all another combination without Fe1 with probability 95%
        '''
        self.run_time_str = '_'.join(str(pd.Timestamp.now()).split(' '))
        if len(self.study.trials_dataframe()) > 0:
            self.next_number_iteration = self.study.trials_dataframe()['number'].max() + 1
        
        count_iteration = self.next_number_iteration + period_length * n_periods
        
        optuna.logging.set_verbosity(optuna.logging.WARNING)
        for i in range(0,n_periods):
            for j in range(0,period_length): 
                clear_output()

                try:
                    print(f'{self.next_number_iteration + 1} of {count_iteration}, number in result {self.next_number_iteration}')
                    print(f'{len(self.droped_features)} features_droped')
                    df=self.study.trials_dataframe()
                    print(f'best_ind = {df[df.value==df.value.max()].index}')
                    print(f'best_result = {df.value.max()}')
                    print(f'last_result = {df.tail(1).value.values[0]}')
                    print(f'metrics std = {df.value.std()}')
                    print(f'metrics_std_last_10 = {df.tail(10).value.std()}')
                    print(f'last_duration = {list(self.study.trials_dataframe().duration)[-1]}')
                    print(f'mean_duration = {self.study.trials_dataframe().duration.mean()}')
                    print(f'for load directory = "{self.log_path}/{self.run_time_str}", epoch = "{i}"')                    
                    print('\n','information_from_scores')
                    print(self.information_from_scores[-1])
                    
                except:
                    print('print_err')
                self.study.optimize(self.objective, n_trials = 1)
                
            self.fi = self.study_features_info()
            if 'ttest_less_p' in list(self.fi):
                self.droped_features.update(self.fi[self.fi.ttest_less_p>p_value_tres].variable.values)
            
            self.save_optuna_state(epoch = i, log_path = self.log_path, run_time_str = self.run_time_str)

    def get_model_params(self, trial_id='best'):
        '''
        Get parametrs models and list of features on trial id
        :param trial_id: trial_id or best
        :return: (feature_list)
        '''
        model_params = self.model_static_params
        feature_list = []
        info = self.iteration_info()
        if isinstance(trial_id, int):
            trial_info = info.loc[trial_id]
        elif trial_id == 'best':
            trial_info = info.sort_values('value', ascending=False).iloc[0]
        else:
            raise Exception('Incorrect trial_id. It must be int or "best"')
        
        if len(self.feature_list) == 0:
            raise Exception('Feature list is empty')
        
        for i in trial_info.index:
            name = i.replace('params_', '')
            if 'params_' in i and name not in self.feature_list:
                model_params[name] = trial_info[i]
        
        for f in self.feature_list:
            if trial_info['params_'+f] == 1:
                feature_list.append(f)
        
        
        return feature_list, model_params