from tqdm.notebook import tqdm
from sklearn.preprocessing import LabelEncoder
from fgb.functions_layer.validation import parent_validation_class

from fgb.core_layer.metrics.core_psi import CorePSI
from fgb.functions_layer.binning.uniform_binning import UniformBinning
from fgb.functions_layer.fillna.fill_na_without_target import FillNaWithoutTarget

import pandas as pd
import numpy as np

class PsiStability(parent_validation_class.ValidationParentClass):
    """
    Class to check psi_stability

    ...

    Attributes
    ----------
    conclusion_color : str
        a result color what set test to model        
    conclusion_text : str
        a result text what set test to model        
    yellow_features : pd.DataFrame
        DataFrame with features who more then yellow_flag        
    red_features : pd.DataFrame
        DataFrame with features who more then red_flag        
    psi_results : pd.DataFrame
        DataFrame with psi all selected features
    """
    
    conclusion_color = None
    conclusion_text = None
    
    yellow_features = None
    red_features = None
    
    psi_results = None
    
    def __init__(self, queries, features, cat_feat):
        """
        Parameters
        ----------
        queries : dict
            queries for selecting set(this set would be check)            
        features : list
            features to check            
        cat_feat : list
            features to special binning
        """
        
        self.queries = queries
        self.features = features
        self.cat_feat = cat_feat
        
    def run(self, df, save_fields, query_train_binned, query_expected, red_flag = 0.2, yellow_flag = 0.1):
        """Run test functions

        Calculated psi for all sets and features

        Parameters
        ----------
        df : pd.DataFrame
            DataFrame for calculating psi
        save_fields : list
            Special fields who need for selecting query           
        query_train_binned : str
            What query will be selected for train binning           
        query_expected : str
            What query will be expected in psi       
        red_flag : float, optional
            If features have psi more then flag, they set to red_features  (default is 0.2 not included)         
        yellow_flag : float, optional
            If features have psi more then flag, they set to yellow_features  (default is 0.1 not included)
        """
        
        # изначальный датасет, который затем будем биннить
        data_binned = df[save_fields + self.features].reset_index(drop=True).copy()
        
        # бинним этот датасет
        data_binned = self.binning_in_test(data_binned, query_train_binned)
        
        self.psi_results = pd.DataFrame()

        # считаем psi по переменным
        for feature in tqdm(self.features):
            try:
                if feature in self.cat_feat:
                    self.psi_results.loc[feature,f'type'] = 'categorical'
                else:
                    self.psi_results.loc[feature,f'type'] = 'numerical'
                
                expected = data_binned.query(self.queries[query_expected])[feature]
                for q in self.queries:
                    actual = data_binned.query(self.queries[q])[feature]

                    psi = CorePSI.psi(expected, actual, binned = True)['psi']
                    self.psi_results.loc[feature,f'psi_{q}'] = psi
            except Exception as e:  # обращение к исключению как к объекту
                    print(f"Произошла ошибка: {e}.", q)
                    
        # считаем вывод по psi
        self.red_features, self.yellow_features = PsiStability.get_result_conclusion(self.psi_results, red_flag = red_flag, yellow_flag = yellow_flag)

    def binning_in_test(self, df, query_train_binned):
        """Functions binning for test(todo move to core or functions level)

        Parameters
        ----------
        df : pd.DataFrame
            DataFrame for binning        
        query_train_binned : str
            What query will be selected for train binning  
            
        Returns
        -------
        pd.DataFrame
            A binned df
        """
        thresholds={}
        fillna_values = {}

        for feature in tqdm(self.features):
            try:
                print(feature)
    
                if feature in self.cat_feat:
                    l = LabelEncoder()
                    l.fit(df[feature])
                    f = pd.Series(l.transform(df[feature]))
                    f_thresholds = pd.Series(l.transform(df.query(self.queries[query_train_binned])[feature]))
                else:
                    f = df[feature]
                    f_thresholds = df.query(self.queries[query_train_binned])[feature]

                thresholds[feature] = UniformBinning.numerical_feature_equisize_binning( f_thresholds
                                                                                     ,bin_size = 0.1
                                                                                     ,min_bin_size = 0.05
                                                                                    )

                fillna_values[feature] = FillNaWithoutTarget.fill_na_wo_target(f_thresholds
                                                                        ,thresholds = thresholds[feature]
                                                                        ,min_share_to_split = 0.05
                                                                        ,merge_na_with= 'biggest_bin'
                                                                        ,default_value=f.median())

                if feature in self.cat_feat:
                    df[feature] = pd.cut(pd.Series(f.values).fillna(fillna_values[feature])
                                         , [-np.inf] + thresholds[feature]
                                         ,labels = thresholds[feature]
                                        )
                else:
                    df[feature] = pd.cut(f.fillna(fillna_values[feature])
                                             , [-np.inf] + thresholds[feature]
                                             ,labels = thresholds[feature]
                                            )
            except Exception as e:  # обращение к исключению как к объекту
                    print(f"Произошла ошибка: {e}.", feature)
                    
        return df
        
    def get_result_text(self):
        """Return conclusion text
            
        Returns
        -------
        str
            Text for result test
        """
        
        return 'added when set rules' #self.conclusion_text

    def get_result_color(self, size=3):        
        """Return conclusion color

        Parameters
        ----------
        size : int
            Size of the lights
            
        Returns
        -------
        str
            return color
        """
        validation_utils.ValidationUtils.plot_lights(self.conclusion_color, size)
        return 'added when set rules' #self.conclusion_color

    def get_result_description(self, return_all=False):
        """Return conclusion color

        Parameters
        ----------
        return_all : bool
            If set return all result for psi
            
        Returns
        -------
        pd.DataFrame
            return features who more the red flag
        pd.DataFrame
            return features who more the yellow flag
        pd.DataFrame, optional
            return all psi
        """
        if return_all:
            return self.red_features, self.yellow_features, self.psi_results
        else:
            return self.red_features, self.yellow_features

    @staticmethod  
    def get_result_conclusion(psi_results, red_flag = 0.2, yellow_flag = 0.1):
        """Function select yellow and red features

        Parameters
        ----------
        psi_results : pd.DataFrame
            datasets with psi. Row - features, column - sets with psi
        red_flag : float, optional
            If features have psi more then flag, they set to red_features  (default is 0.2 not included)         
        yellow_flag : float, optional
            If features have psi more then flag, they set to yellow_features  (default is 0.1 not included)
            
        Returns
        -------
        pd.DataFrame
            return features who more the red flag
        pd.DataFrame
            return features who more the yellow flag
        """
        temp_yellow = (psi_results.select_dtypes(include=['float64', 'int64']) > yellow_flag).sum(axis=1)
        yellow_features = psi_results.loc[temp_yellow[temp_yellow > 0].index]
        
        temp_red = (psi_results.select_dtypes(include=['float64', 'int64']) > red_flag).sum(axis=1)
        red_features = psi_results.loc[temp_red[temp_red > 0].index]
        
        return red_features, yellow_features