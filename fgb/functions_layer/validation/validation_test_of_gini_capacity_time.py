from matplotlib import pyplot as plt
from tqdm.notebook import tqdm
from fgb.functions_layer.validation import parent_validation_class

import pandas as pd
import numpy as np

from fgb.functions_layer.metrics import simple_metrics
from fgb.utils.validation import validation_utils

class GiniCapacityTime(parent_validation_class.ValidationParentClass):
    """
    Class to compare gini in time

    ...

    Attributes
    ----------
    conclusion_color : str
        a result color what set test to model        
    conclusion_text : str
        a result text what set test to model
                
    gini_table : pd.DataFrame
        DataFrame with gini for all queries        
    gini_table_results_for_query : pd.DataFrame
        DataFrame with flag of query red_flag, yellow_flag
    queries : dict
        Dict with queries
    target_column : str
        Target column    
    score_column : str
        Score column    
    time_interval_column : str
        Column with time(month or year or day or etc.)
    red_flag : float
        Gini red border
    yellow_flag : float
        Gini yellow border
    """
    
    conclusion_color = None
    conclusion_text = None
    
    gini_table = None
    gini_table_results_for_query = None
    queries = None
    target_column = None
    score_column = None
    time_interval_column = None
    red_flag = None
    yellow_flag = None
    
    def __init__(self, queries, target_column, score_column, time_interval_column, red_flag=0.25, yellow_flag=0.45):
        """
        Parameters
        ----------
        queries : dict
            queries for selecting set(this sets would be check)            
        target_column : str
            target column            
        score_column : str
            score column         
        time_interval_column : str
            Column with time(month or year or day or etc.)
        red_flag : float
            Gini red border
        yellow_flag : float
            Gini yellow border
        """
        self.queries = queries
        self.target_column = target_column
        self.score_column = score_column
        self.time_interval_column = time_interval_column
        self.red_flag = red_flag
        self.yellow_flag = yellow_flag
        
    def run(self, df):
        """Run test functions

        Calculated gini for set and column

        Parameters
        ----------
        df : pd.DataFrame
            DataFrame for calculating gini
        """ 
        gini_table = []
        gini_table_results_for_query = []

        for q in self.queries:
            # все даты
            all_month = sorted(df.query(self.queries[q])[self.time_interval_column].unique())
            for t in tqdm(all_month):
                try:
                    y_true = df.query(self.queries[q]).query(f'{self.time_interval_column} == "{t}"')[self.target_column]
#                     print(q, t, f'{self.time_interval_column} == {t}', self.target_column)
                    y_pred = df.query(self.queries[q]).query(f'{self.time_interval_column} == "{t}"')[self.score_column]
                    
                    
                    gini = simple_metrics.SMFunc.gini(y_true,y_pred)
                    if type(gini)!=str:
                        gini_table.append({'query':q, 'gini':gini, 'time_interval':t, 'count_all':len(y_true), 'count_bad':sum(y_true), 'DR':np.mean(y_true), 'error':gini})
                    else:
                        gini_table.append({'query':q, 'gini':0, 'time_interval':t, 'count_all':len(y_true), 'count_bad':sum(y_true), 'DR':np.mean(y_true), 'error':gini})
                        

                except Exception as e:  # обращение к исключению как к объекту
                        print(f"Произошла ошибка: {e}.", q)
                    
        self.gini_table = pd.DataFrame(gini_table)
        
        # целевых событий не менее 100
        self.gini_table['used'] = 0
        self.gini_table.loc[self.gini_table['count_bad'] >= 100, 'used'] = 1
        
        self.gini_table_results_for_query = self.get_percent_conclusion(self.queries, self.gini_table, self.red_flag, self.yellow_flag)
        
        self.conclusion_color, self.conclusion_text = self.get_result_conclusion(self.gini_table_results_for_query)
    
    @staticmethod     
    def get_percent_conclusion(queries, gini_table, red_flag, yellow_flag):
        """Get percent conclusion

        Parameters
        ----------
        queries : dict
            Dict with queries
        gini_table : pd.DataFrame
            DataFrame with gini for all queries
        red_flag : float
            Gini red border
        yellow_flag : float
            Gini yellow border
            
        Returns
        -------
        pd.DataFrame
            DataFrame with gini for all queries and time with proportion(from 0 to 1)
        """
        gini_table_results_for_query = []
        for q in queries:
            temp = gini_table[(gini_table['query'] == q) & (gini_table['used'] == 1)]
            if len(temp) > 0:
                less_red = sum(temp['gini'] < red_flag)/len(temp)
                less_yellow = sum(temp['gini'] < yellow_flag)/len(temp)
                gini_table_results_for_query.append({'query':q, 'gini_less_red':less_red, 'gini_less_yellow':less_yellow, 'error':''})
            else:
                gini_table_results_for_query.append({'query':q, 'gini_less_red':0, 'gini_less_yellow':0, 'error':'not_have_gini'})

        return pd.DataFrame(gini_table_results_for_query)
        
    @staticmethod   
    def get_result_conclusion(gini_table_results_for_query):
        """Get percent conclusion

        Parameters
        ----------
        gini_table_results_for_query : dict
            DataFrame with flag of query red_flag, yellow_fla
            
        Returns
        -------
        str, str
            return color and text of all set
        """
        if max(gini_table_results_for_query['gini_less_red']) >= 0.3:
            color = 'r'
            text = 'Gini < 25% в 30% и более от анализируемых периодов.'
        elif (max(gini_table_results_for_query['gini_less_yellow']) > 0.3) or (max(gini_table_results_for_query['gini_less_red']) > 0):
            color = 'y'
            text = 'Gini ϵ [25%; 45%) в 30% и более от анализируемых периодов или Gini < 25% хотя бы за один период.'
        else:
            color = 'g'
            text = 'Разделяющая способность модели на всех месяцах выборки выше 45%.'
        
        return color, text
        
    def get_result_text(self):
        """Return conclusion text
            
        Returns
        -------
        str
            Text for result test
        """
        return self.conclusion_text

    def get_result_color(self, size=3):
        """Return conclusion color

        Parameters
        ----------
        size : int
            Size of the lights
            
        Returns
        -------
        str
            return color
        """
        validation_utils.ValidationUtils.plot_lights(self.conclusion_color, size)
        return self.conclusion_color

    def get_result_description(self, group='all', text_template='Gini по месяцам ', time_interval_name='Month'):
        """Return description

        This function plot graphics for time column


        Parameters
        ----------
        group : dict or str
            This field set setting of graph:
                1) 'all' - plot all graph
                2) 'once' - plot graph for every query
                3) dict - dict with queries and count type, example: 
                   group={
                       'вся выборка':{'queries':['all'], 'count_type':'all'},
                       'детально':{'queries':['train', 'valid', 'test', 'oot', 'o_oot'], 'count_type':'train'}
                   }
        text_template : str
            Title of plot graph
        time_interval_name : str
            Xlabel
            
        Returns
        -------
        pd.DataFrame, pd.DataFrame
            return table for month and table of proportion(from 0 to 1) of more or less then flags
        """
        group_queries = {}
        
        if group == 'all':
            months=self.gini_table['time_interval'].astype(str)

            title = f'{text_template}для всех'
            fig = plt.subplots(figsize = (20, 10))
            gridsize = (5, 5)
            plt.suptitle(ha='center',t=f'\n{title}', fontsize=16)

            ax_02 = plt.subplot2grid(gridsize, (0, 1), colspan=3, rowspan=3)
            ax_02.set_xticklabels(months.to_list(),minor=False,rotation = 45)
            ax_02.set_xlabel(time_interval_name, fontsize=13)
        
            for q in self.queries:
                ax_02.plot(self.gini_table[self.gini_table['query'] == q][f'time_interval'].astype(str), self.gini_table[self.gini_table['query'] == q][f'gini'], linewidth=3.0, marker='o',label=f'gini on {q}')

            ax_02.axhline(y=self.yellow_flag,linestyle = '--',lw = 2,color = 'green')
            ax_02.axhline(y=self.red_flag,linestyle = '--',lw = 2,color = 'red')

            ax_02.set_ylabel(u'Gini', fontsize=13)

            plt.legend()
            plt.grid(True)
        
        elif group == 'once':            
            for q in self.queries:
                temp = self.gini_table[self.gini_table['query'] == q]
                range_len = range(len(temp))
                months = temp['time_interval'].astype(str)
                counts = temp['count_all']

                title = f'{text_template}{q}'
                fig = plt.subplots(figsize = (20, 10))
                gridsize = (5, 5)
                plt.suptitle(ha='center',t=f'\n{title}', fontsize=16)

                ax_01 = plt.subplot2grid(gridsize, (0, 1), colspan=3, rowspan=3)
                ax_01.set_xticks(range_len)
                ax_01.set_xticklabels(months.to_list(),minor=False,rotation = 45)

                ax_01.set_xlabel(time_interval_name, fontsize=13)
                ax_01.bar(range_len, counts, color='lightgray', zorder=2)

                ax_02 = ax_01.twinx()
                #         print(dataset_segm, dataset_set, temp[f'ginis_def_9_90_{dataset_set}_{segment}'])
                ax_02.plot(temp['time_interval'].astype(str), temp[f'gini'], linewidth=3.0, marker='o',label=f'{text_template} {q}')

                ax_02.axhline(y=self.yellow_flag,linestyle = '--',lw = 2,color = 'green')
                ax_02.axhline(y=self.red_flag,linestyle = '--',lw = 2,color = 'red')

                ax_01.set_ylabel(u'Count', fontsize=13)
                ax_02.set_ylabel(u'Gini', fontsize=13)

                plt.legend()
                plt.grid(True)
                
        elif len(group) > 0:                
            for g in group:
                temp = self.gini_table[self.gini_table['query'] == group[g]['count_type']]
                range_len = range(len(temp))
                months = temp['time_interval'].astype(str)
                counts = temp['count_all']
                #print(counts)

                title = f'{text_template}{g}'
                fig = plt.subplots(figsize = (20, 10))
                gridsize = (5, 5)
                plt.suptitle(ha='center',t=f'\n{title}', fontsize=16)

                ax_01 = plt.subplot2grid(gridsize, (0, 1), colspan=3, rowspan=3)
                ax_01.set_xticks(range_len)
                ax_01.set_xticklabels(months.to_list(),minor=False,rotation = 45)

                ax_01.set_xlabel(time_interval_name, fontsize=13)
#                 for q in group[g]['queries']:
                ax_01.bar(months.astype(str), counts, color='lightgray', zorder=2)

                ax_02 = ax_01.twinx()
                for q in group[g]['queries']:
                    ax_02.plot(self.gini_table[self.gini_table['query'] == q][f'time_interval'].astype(str), self.gini_table[self.gini_table['query'] == q][f'gini'], linewidth=3.0, marker='o',label=f'gini on {q}')

                ax_02.axhline(y=self.yellow_flag,linestyle = '--',lw = 2,color = 'green')
                ax_02.axhline(y=self.red_flag,linestyle = '--',lw = 2,color = 'red')

                ax_01.set_ylabel(f'Count {group[g]["count_type"]}', fontsize=13)
                ax_02.set_ylabel(u'Gini', fontsize=13)

                plt.legend()
                plt.grid(True)
        
        return self.gini_table, self.gini_table_results_for_query