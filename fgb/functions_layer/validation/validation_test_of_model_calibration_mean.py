from tqdm import tqdm
from matplotlib import pyplot as plt
import json
import pickle

from fgb.functions_layer.validation import parent_validation_class

import pandas as pd
import numpy as np

class ModelCalibrationMean(parent_validation_class.ValidationParentClass):
    """
    Class to compare gini in time
    
    # почему сигма так выглядит, получается односторонний z-test, остается вопрос почему они используют его?
    # https://stackoverflow.com/questions/12412895/how-to-calculate-probability-in-a-normal-distribution-given-mean-standard-devi

    # про z-метрику
    # https://habr.com/ru/articles/653363/

    ...

    Attributes
    ----------
    conclusion_color : str
        a result color what set test to model        
    conclusion_text : str
        a result text what set test to model

    target_column : str
        target column
    calib_score_column : str
        calib score column
    calib_table : str
        table with calibration parametrs
    """

    conclusion_color = None
    conclusion_text = None
    
    target_column = None
    calib_score_column = None
    calib_table = None
    
    def __init__(self, target_column, calib_score_column):
        """
        Parameters
        ----------     
        target_column : str
            target column
        calib_score_column : str
            calib score column
        """
        self.target_column = target_column
        self.calib_score_column = calib_score_column
        
    def run(self, df):
        """Run test functions(from old framework)

        Calculated gini for set and column

        Parameters
        ----------
        df : pd.DataFrame
            DataFrame for calculating gini
        """ 
        cnt = len(df)

        m_p = df[self.calib_score_column]

        m_p = m_p.sum()/m_p.count()
        m_f = df[self.target_column].mean()

        #sigma on calib predict values
        sig = ModelCalibrationMean.sigma(m=m_p,n=cnt)

        l_99_g, u_99_g = ModelCalibrationMean.conf_interval(sigma=sig,m=m_p,interval_vl=99)
        l_95_g, u_95_g = ModelCalibrationMean.conf_interval(sigma=sig,m=m_p,interval_vl=95)

        self.calib_table = {'row_num':0,'mean_pred':m_p,'mean_fact':m_f,'cnt':cnt
                                        ,'lower_99':l_99_g,'upper_99':u_99_g
                                        ,'lower_95':l_95_g,'upper_95':u_95_g}

    @staticmethod
    def sigma(m,n):
        """Get standart deviation for z metric(from old framework)

        Parameters
        ----------
        m : float
            mean predict
        n : int
            count
            
        Returns
        -------
        float
            value of standart deviation
        """
        return np.sqrt(m*(1-m)/n)
    
    
    @staticmethod
    def conf_interval(sigma,m,interval_vl):
        """Get interval by sigma(from old framework)

        Parameters
        ----------
        sigma : float
            sigma of distribution
        m : float
            mean predict
        interval_vl : float
            percent of want get
            
        Returns
        -------
        float, float
            left and right border
        """
        dict_inter = {99:2.58,95:1.96,90:1.64}
        i = dict_inter.get(interval_vl)

        return  m-i*sigma,  m+i*sigma
    
    @staticmethod
    def get_result_conclusion(dict_nm):
        """Get conclusion(from old framework)

        Parameters
        ----------
        dict_nm : float
            dict with parametrs
            
        Returns
        -------
        str, str, str
            result test ru, result test eng, result color
        """
        
        dict_calib_txt_ru = {'3g':'Средний факт входит в 95% дов. интервал\nдля среднего прогнозного значения\nРекомендуется внедрить модель'
                         ,'2y':'Средний факт входит в 99% дов. инт.\nно не входит в 95% дов. инт.\nМодель может быть внедрена'
                         ,'1r':'Средний факт не входит в 99% дов. интервал\nдля среднего прогнозного значения\nРекомендуется обновить калибровку'}

        dict_calib_txt_eng = {'3g':'The avg fact falls within the 95% conf. interval\nfor the predicted mean value\nThe model is recommended for implementation'
                         ,'2y':'The avg fact falls within the 99% conf. interval\nbut is not included in the 95% conf. interval\nThe model can be implemented'
                         ,'1r':'The avg fact is not included in the 99% conf. interval\nfor the predicted mean value\nIt is recommended to update the calibration'}

        #Color for all test
        total_color = '3g'

        for k,v in dict_nm.items():
            if v['lower_95'] <= v['mean_fact'] <= v['upper_95']:        
                total_color = '3g' if '3g' < total_color else total_color
            elif v['lower_99'] <= v['mean_fact'] <= v['upper_99']:
                total_color = '2y' if '2y' < total_color else total_color
            else:
                total_color = '1r' if '1r' < total_color else total_color        

        return dict_calib_txt_ru.get(total_color), dict_calib_txt_eng.get(total_color), total_color[1:]
    
    @staticmethod
    def test_plot(dict_coef,txt_calib,color_calib,txt_title,is_final):
        """Plot boxplot(from old framework)

        Parameters
        ----------
        dict_coef : float
            dict with parametrs
        txt_calib : float
            dict with parametrs
        color_calib : float
            dict with parametrs
        txt_title : float
            dict with parametrs
        is_final : float
            dict with parametrs
            
        Returns
        -------
        str, str, str
            result test ru, result test eng, result color
        """
        cnt_prod = len(dict_coef.keys())

        #Parametrs for plt
        dict_grid = {3:(12,4),2:(8,4),1:(8,4)}
        dict_light = {3:(9,0),2:(4,0),1:(4,0)}
        dict_text = {3:(9,1),2:(4,1),1:(4,1)}

        light_row = 2 if cnt_prod==1 or cnt_prod==2 else 3
        ax3_y_txt = 0.4 if cnt_prod==1 or cnt_prod==2 else 0

        size = (10,10)
        gridsize = dict_grid.get(cnt_prod)
        fig = plt.subplots(figsize = size)

        plt.suptitle(txt_title, fontsize=16)

        row_size = 4 if is_final=='y' else 5

        #Отрисовка для разных продуктов
        for k,v in dict_coef.items():
            x = v['row_num']
            s = (0, 1)

            ax = plt.subplot2grid(gridsize, s, colspan=2, rowspan=row_size)
            ax.set_title(f"Calibration for {k[:]}")

            ax.set_xlim([0.4, 0.6])

            upper_bound = v['mean_fact'] if v['mean_fact']>v['upper_99'] else v['upper_99']
            lower_bound = v['mean_fact'] if v['mean_fact']<v['lower_99'] else v['lower_99']

            ax.plot([0.5,0.5],[lower_bound*0.99,upper_bound*1.01], linewidth=3.0,color='black')

            #Отрисовка вертикальных линий для 99%
            ax.axhline(y=v['lower_99'],xmin=0.4, xmax=0.6,linestyle = '--',lw = 2,color = 'red',label='99%')
            ax.axhline(y=v['upper_99'],xmin=0.4, xmax=0.6,linestyle = '--',lw = 2,color = 'red')

            #Отрисовка вертикальных линий для 95%
            ax.axhline(y=v['lower_95'],xmin=0.4, xmax=0.6,linestyle = '--',lw = 2,color = 'y',label='95%')
            ax.axhline(y=v['upper_95'],xmin=0.4, xmax=0.6,linestyle = '--',lw = 2,color = 'y')

            #Зарисовка для 95%
            ax.fill_between(x=[0.475, 0.525], y1=v['lower_95'] ,y2=v['upper_95'],color = 'green', alpha=0.2)

            #Зарисовка для 99%
            ax.fill_between(x=[0.475, 0.525], y1=v['lower_99'] ,y2=v['lower_95'],color = 'y', alpha=0.2)

            ax.fill_between(x=[0.475, 0.525], y1=v['upper_99'] ,y2=v['upper_95'],color = 'y', alpha=0.2)

            #Выводим m&fact
            ax.axhline(y=v['mean_pred'],xmin=0.4, xmax=0.6,linestyle = '--',lw = 2,color = 'grey'  ,label=f"mean:{round(v['mean_pred'],4)}")
            ax.axhline(y=v['mean_fact'],xmin=0.45, xmax=0.55,linestyle = '-',lw = 4,color = 'green' ,label=f"fact:{round(v['mean_fact'],4)}")

            #Убираем шкалу с оси x
            ax.xaxis.set_visible(False)

            plt.legend(loc=1)

        #Light and Text for final list
        if is_final=='y':
            ax2 = test_lights(gridsize=gridsize,ax_loc=dict_light.get(cnt_prod),color=color_calib,row=light_row)

            plt.subplots_adjust(wspace=0.4, hspace=1.7)

            ax3 = plt.subplot2grid (gridsize, dict_text.get(cnt_prod), colspan=1, rowspan=2)
            ax3.text(-0.2, ax3_y_txt, f'{txt_calib}', fontsize=12)
            ax3.axis('off')   

        return fig