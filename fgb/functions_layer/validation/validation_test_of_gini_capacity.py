from tqdm.notebook import tqdm
from fgb.functions_layer.validation import parent_validation_class

import pandas as pd
import numpy as np

from fgb.functions_layer.metrics import simple_metrics
from fgb.utils.validation import validation_utils

class GiniCapacity(parent_validation_class.ValidationParentClass):
    """
    Class to gini value level check

    '''

    Attributes
    ----------
    conclusion_color : str
        a result color what set test to model
    conclusion_text : str
        a result text what set test to model
    gini_table : pd.DataFrame
        DataFrame with gini for all set
    baddest : pdDataFrame
        Baddest gini in the sets
    queries : dict
        Sets for test
    target_column : str
        Target column for calculating gini
    score_name : str
        Score column for calculating gini
    """

    conclusion_color = None
    conclusion_text = None

    gini_table = None
    baddest = None
    queries = None
    target_column = None
    score_column = None

    def __init__(self, queries, target_column, score_column):
        """
        Parametrs
        ---------
        queries : dict
            queries for selecting set(this set would be check)
        target_column : str
            Target column for calculating gini
        score_column : str
            Score column for calculating gini
        """
        self.queries = queries
        self.target_column = target_column
        self.score_column = score_column

    def run(self, df, fontsize=16, fontsize_legend=14):
        """Run test functions

        Calculating gini for all sets

        Parametrs
        ---------
        df : pd.DataFrame
            DataFrame for calculating gini
        """
        gini_table = []

        for q in self.queries:
            try:
                y_true = df.query(self.queries[q])[self.target_column]
                y_pred = df.query(self.queries[q])[self.score_column]
                simple_metrics.SMFunc.plot_roc(y_true,y_pred, title=f'на выборке {q}', fontsize=fontsize, fontsize_legend=fontsize_legend)
                gini = simple_metrics.SMFunc.gini(y_true,y_pred)
                color, text = self.get_result_conclusion(gini)

                validation_utils.ValidationUtils.plot_lights(color, 3)
                print(text[0])
                gini_table.append({'query':q, 'gini':gini, 'color':color, 'text':text})

            except Exception as e:
                print(f'Произошла ошибка: {e}', q)

        self.gini_table = pd.DataFrame(gini_table)

        self.baddest = self.gini_table.loc[self.gini_table['gini'].argmin()]

        self.conclusion_color, self.conclusion_text = self.get_result_conclusion(self.baddest['gini'])

    def get_result_text(self, lang='ru'):
        """Get text

        Parametrs
        ---------
        lang : str, optional
            Language of return text
        Returns
        -------
        str
            return conclusion text
        """
        if lang=='ru':
            return print(self.conclusion_text[0])
        else:
            return print(self.conclusion_text[1])

    def get_result_colot(self, size=3):
        """Return conclusion color

        Parametrs
        ---------
        size : int
            Size of the lights

        Returns
        -------
        str
            return color
        """
        validation_utils.ValidationUtils.plot_lights(self.conclusion_color, size)
        return self.conclusion_color

    def get_result_descripton(self):
        """Returns result for every set

        Returns
        -------
        pd.DataFrame
            return gini for set
        """
        return self.gini_table

    @staticmethod
    def get_result_conclusion(gini):
        """Return color and text for gini value

        Parametrs
        ---------
        gini : int
            value of gini
        
        Returns
        -------
        str
            return color
        str
            return text
        """
        if gini < 0.3:
            text = f'Модель обладает низкой разделяющей способностью. Модель нельзя использовать',\
                    'GUNU is lower 30%:the model with low discriminative power.The model cannot be used'
            color = 'r'
        elif gini < 0.5:
            text = f'Модель обладает достаточной разделяющей способностью. Модель рекомендуется к использованию',\
                    'GUNU between 30% and 50%:the model has a sufficient discriminative power.The model can be implemented'
            color = 'y'
        else:
            text = f'Модель обладает высокой разделяющей способностью. Модель рекомендуется к использованию',\
                    'GUNU from 50%:the model has a gight discriminative power.The model is recommended for implemented'
            color = 'g'
        
        return color, text