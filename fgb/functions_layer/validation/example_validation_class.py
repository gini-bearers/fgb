from fgb.functions_layer.validation import parent_validation_class
from fgb.utils.log import log_helper
from fgb.utils.validation import validation_utils

class ValidationTest_0_0(parent_validation_class.ValidationParentClass):

    test_name = None

    def __init__(self, test_name='Пример теста', logger=''):
        if logger != '':
            self.test_logger = logger
        else:
            self.test_logger = log_helper.LogHelpers('test_0')

        self.test_logger.write_message('Инициализация теста номер 0')
        self.test_name = test_name    

    def run(self):
        self.test_logger.write_message('Запуск теста номер 0')
        print('run method')
        self.test_logger.write_message('Завершение теста номер 0')
        # self.test_logger.close()

    def result_conclusion(self):
        return 'Модель проходит все метрики'

    def result_color(self):
        return validation_utils.ValidationUtils.plot_lights('g')

    def result_description(self):
        return 'Большое описание сложного метода и графики, которые надо вставить в отчет'