class ValidationParentClass():

    def __init__(self, test_name=''):
        self.test_name = test_name  

    def run(self):
        print('release run method')

    def result_conclusion(self):
        print('release result_conclusion method')

    def result_color(self):
        print('release result_color method')

    def result_description(self):
        print('release result_description method')