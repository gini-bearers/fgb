import seaborn as sns
import pandas as pd
from tqdm import tqdm
from matplotlib import pyplot as plt

from fgb.utils.validation import validation_utils
from fgb.functions_layer.validation import parent_validation_class
from fgb.functions_layer.metrics import simple_metrics

class GiniCapacityCompare(parent_validation_class.ValidationParentClass):
    """
    Class to compare gini capacity

    ...

    Attributes
    ----------
    conclusion_color : str
        a result color what set test to model        
    conclusion_text : str
        a result text what set test to model
                
    gini_table : pd.DataFrame
        DataFrame with gini for all queries        
    queries : dict
        Dict with queries       
    reference_query : str
        The query against which gini is evaluated
    target_column : str
        Target column    
    score_column : str
        Score column
    """
    
    conclusion_color = None
    conclusion_text = None
    
    gini_table = None
    queries = None
    reference_query = None
    target_column = None
    score_column = None
    
    def __init__(self, queries, target_column, score_column):
        """
        Parameters
        ----------
        queries : dict
            queries for selecting set(this sets would be check)            
        target_column : str
            target column            
        score_column : str
            score column
        """
        self.queries = queries
        self.target_column = target_column
        self.score_column = score_column
        
    def run(self, df, reference_query=None):
        """Run test functions

        Calculated gini for set

        Parameters
        ----------
        df : pd.DataFrame
            DataFrame for calculating gini
        reference_query : str
            The query against which gini is evaluated
        """ 
        gini_table = []

        for q in self.queries:
            try:
                y_true = df.query(self.queries[q])[self.target_column]
                y_pred = df.query(self.queries[q])[self.score_column]
                gini = simple_metrics.SMFunc.gini(y_true,y_pred)
                gini_table.append({'query':q, 'gini':gini*100})

            except Exception as e:  # обращение к исключению как к объекту
                    print(f"Произошла ошибка: {e}.", q)
                    
        self.gini_table = pd.DataFrame(gini_table)
        
        self.gini_table = GiniCapacityCompare.get_result_conclusion(self.gini_table, reference_query)

    @staticmethod  
    def get_result_conclusion(gini_table, reference_query=None):
        """Get conclusion

        Parameters
        ----------
        gini_table : pd.DataFrame
            DataFrame with gini for all queries        
        reference_query : str
            What query will be selected for train binning  
            
        Returns
        -------
        pd.DataFrame
            DataFrame with gini for all queries with added relative
        """
        gini_table_temp = gini_table.copy()
        if reference_query:
            reference_gini = list(gini_table_temp.loc[gini_table_temp['query'] == reference_query, 'gini'])[0]
        else:
            reference_gini = max(gini_table_temp['gini'])
        
        gini_table_temp['gini_relative_percent'] = round((1-(gini_table_temp['gini']/reference_gini))*100,2)
        gini_table_temp['gini_relative_absolut'] = round((reference_gini-gini_table_temp['gini']),2)
            
        return gini_table_temp

    @staticmethod
    def test_plot(gini_table, graph_name):
        """Plot gini table

        Parameters
        ----------
        gini_table : pd.DataFrame
            DataFrame with gini for all queries
        """
        fig = plt.subplots(figsize = (10,10))
        gridsize = (5, 5)

        plt.suptitle(x=0.5,y=1,ha='center',t=graph_name, fontsize=16)

        GiniCapacityCompare.axes_stability(axes_nm='ax_01',coor=(0,0),title='Values of gini'
                       ,x_vl=gini_table['query'],y_vl=round(gini_table['gini'],2))

        GiniCapacityCompare.axes_stability(axes_nm='ax_02',coor=(0, 3),title='Δ gini (%)'
                       ,x_vl=gini_table['query'],y_vl=gini_table['gini_relative_percent'],tp='relat')

        GiniCapacityCompare.axes_stability(axes_nm='ax_03',coor=(0, 6),title='Δ gini (abs)'
                       ,x_vl=gini_table['query'],y_vl=gini_table['gini_relative_absolut'],tp='abs')

    @staticmethod 
    def axes_stability(axes_nm,coor,title,x_vl,y_vl,is_indent=True,tp=None,gridsize=(5, 9)):        
        """
        Plot axes(from old framework)
        """
        axes_nm = plt.subplot2grid(gridsize, coor, colspan=3, rowspan=3)
        axes_nm = sns.barplot(x=x_vl,y=y_vl, palette = 'magma')
        axes_nm.set_title(title)    

        axes_nm.set_xticklabels(x_vl,minor=False,rotation = 45)

        GiniCapacityCompare.add_annotate(ax_nm=axes_nm)

        if is_indent:
            plt.subplots_adjust(wspace=2, hspace=2)    

        if tp=='relat':
            axes_nm.axhline(y=10,linestyle = '--',lw = 2,color = 'y')
            axes_nm.axhline(y=20,linestyle = '--',lw = 2,color = 'red')
        elif tp=='abs':
            axes_nm.axhline(y=5,linestyle = '--',lw = 2,color = 'y')
            axes_nm.axhline(y=10,linestyle = '--',lw = 2,color = 'red')
    
    @staticmethod 
    def add_annotate(ax_nm,f=''):
        """
        Add annotate(from old framework)
        """
        for p in ax_nm.patches:
            ax_nm.annotate(format(p.get_height(), f), 
                           (p.get_x() + p.get_width() / 2., p.get_height()), 
                           ha = 'center', va = 'center', 
                           xytext = (0, 9), 
                           textcoords = 'offset points')