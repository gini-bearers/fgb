from tqdm.notebook import tqdm
from sklearn.preprocessing import LabelEncoder
from fgb.functions_layer.validation import parent_validation_class

from fgb.core_layer.metrics.core_psi import CorePSI
from fgb.functions_layer.binning.uniform_binning import UniformBinning
from fgb.functions_layer.fillna.fill_na_without_target import FillNaWithoutTarget

import pandas as pd
import numpy as np

class BinsStability(parent_validation_class.ValidationParentClass):
    """
    Class to check bins stability

    ...

    Attributes
    ----------
    conclusion_color : str
        a result color what set test to model        
    conclusion_text : str
        a result text what set test to model        
    yellow_features : pd.DataFrame
        DataFrame with features who more then yellow_flag        
    red_features : pd.DataFrame
        DataFrame with features who more then red_flag        
    thresholds : pd.DataFrame
                
    l_decoders : pd.DataFrame
        DataFrame with psi all selected features        
    profile_test_months_array : pd.DataFrame
        DataFrame with psi all selected features        
    save_dr_name : pd.DataFrame
        DataFrame with psi all selected features        
    save_count_name : pd.DataFrame
        DataFrame with psi all selected features
    """
    
    conclusion_color = None
    conclusion_text = None
    
    yellow_features = None
    red_features = None    
    
    thresholds = None
    l_decoders = None
    profile_test_months_array = None
    save_dr_name = None
    save_count_name = None
    
    def __init__(self, queries, features, cat_feat):
        """
        Parameters
        ----------
        queries : dict
            queries for selecting set(this sets would be check)            
        features : list
            features to check            
        cat_feat : list
            features to special binning
        """
        self.queries = queries
        self.features = features
        self.cat_feat = cat_feat
        
    def run(self, df, save_fields, query_train_binned, time_column, target_column):
        """Run test functions

        Calculated psi for all sets and features

        Parameters
        ----------
        df : pd.DataFrame
            DataFrame for calculating psi
        save_fields : list
            Special fields who need for selecting query           
        query_train_binned : str
            What query will be selected for train binning           
        time_column : str
            Column to use a time column(this field use as timeline)       
        target_column : str
            Column with target, for get dr and count of bad
        """
        data_binned = df[save_fields + self.features].reset_index(drop=True).copy()
        
        print('start_binning')
        data_binned, self.thresholds, self.l_decoders = self.binning_in_test(data_binned, query_train_binned)
        print('end_binning')
        
        self.profile_test_months_array = {}
        self.save_dr_name = []
        self.save_count_name = []

        print('start_time_calculating')
        for feature in tqdm(self.features):
            profile_test_months = pd.DataFrame()

            for time_column_i in sorted(list(data_binned[time_column].unique())):
                try:
                    profile_test_temp = data_binned[(data_binned[time_column] == time_column_i)].groupby(feature).agg({target_column:['count', 'sum', 'mean']}).reset_index()
                    profile_test_temp.columns = ['bin', 'count_'+str(time_column_i), 'sum_'+str(time_column_i), 'mean_'+str(time_column_i)]
                    self.save_dr_name.append('mean_'+str(time_column_i))
                    self.save_count_name.append('count_'+str(time_column_i))

                    if len(profile_test_months) == 0:
                        profile_test_months = profile_test_temp
                    else:
                        profile_test_months = profile_test_months.merge(profile_test_temp, on='bin', how='outer')

                except Exception as e:  # обращение к исключению как к объекту  
                    print(f"Произошла ошибка: {e}.", feature, time_column_i)

            self.profile_test_months_array[feature] = profile_test_months
        print('end_time_calculating')
        self.save_dr_name = sorted(list(set(self.save_dr_name)))
        self.save_count_name = sorted(list(set(self.save_count_name)))
        

    def binning_in_test(self, df, query_train_binned):        
        """Functions binning for test(todo move to core or functions level)

        Parameters
        ----------
        df : pd.DataFrame
            DataFrame for binning        
        query_train_binned : str
            What query will be selected for train binning  
            
        Returns
        -------
        pd.DataFrame
            A binned df
        """
        thresholds={}
        l_decoders = {}
        fillna_values = {}

        for feature in tqdm(self.features):
            try:    
                if feature in self.cat_feat:
                    l = LabelEncoder()
                    l.fit(df[feature])
                    l_decoders[feature] = l
                    f = pd.Series(l.transform(df[feature]))
                    f_thresholds = pd.Series(l.transform(df.query(self.queries[query_train_binned])[feature]))
                else:
                    f = df[feature]
                    f_thresholds = df.query(self.queries[query_train_binned])[feature]
                    

                thresholds[feature] = UniformBinning.numerical_feature_equisize_binning( f_thresholds
                                                                                     ,bin_size = 0.1
                                                                                     ,min_bin_size = 0.05
                                                                                    )

                fillna_values[feature] = FillNaWithoutTarget.fill_na_wo_target(f_thresholds
                                                                        ,thresholds = thresholds[feature]
                                                                        ,min_share_to_split = 0.05
                                                                        ,merge_na_with= 'biggest_bin'
                                                                        ,default_value=f.median())

                if feature in self.cat_feat:
                    df[feature] = pd.cut(pd.Series(f.values).fillna(fillna_values[feature])
                                         , [-np.inf] + thresholds[feature]
                                         ,labels = thresholds[feature]
                                        )
                else:
                    df[feature] = pd.cut(f.fillna(fillna_values[feature])
                                             , [-np.inf] + thresholds[feature]
                                             ,labels = thresholds[feature]
                                            )
            except Exception as e:  # обращение к исключению как к объекту
                    print(f"Произошла ошибка: {e}.", feature)
                    
        return df, thresholds, l_decoders
        
    def get_result_text(self):
        """Return conclusion text
            
        Returns
        -------
        str
            Text for result test
        """
        
        return 'added when set rules' #self.conclusion_text

    def get_result_color(self, size=3):        
        """Return conclusion color

        Parameters
        ----------
        size : int
            Size of the lights
            
        Returns
        -------
        str
            return color
        """
        validation_utils.ValidationUtils.plot_lights(self.conclusion_color, size)
        return 'added when set rules' #self.conclusion_color

    def get_result_description(self):
        return 'added when set rules' #self.gini_table