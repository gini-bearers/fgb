from tqdm import tqdm
from matplotlib import pyplot as plt
import json
import pickle

from fgb.utils.validation import validation_utils
from fgb.functions_layer.validation import parent_validation_class

from fgb.functions_layer.metrics import simple_metrics

import pandas as pd
import numpy as np

class ModelUpliftForward(parent_validation_class.ValidationParentClass):
    """
    Class to test uplift model

    ...

    Attributes
    ----------
    conclusion_color : str
        A result color what set test to model        
    conclusion_text : str
        A result text what set test to model

    queries : dict
        Target column
    target_column : str
        Target column
    gini_table : pd.DataFrame
        Dataframe with result calculating model
    sorted_features_list : list
        List with order for learning model
    references_gini : dict
        Main gini for compare by sets
    gini_table_color_text : pd.DataFrame
        DataFrame for result color to set
    """
    
    conclusion_color = None
    conclusion_text = None
    
    queries = None
    target_column = None
    gini_table = None
    sorted_features_list = None
    references_gini = {}
    gini_table_color_text = None
    
    def __init__(self, queries, target_column, sorted_features_list):
        """
        Parameters
        ----------     
        queries : str
            Dict with queries
        target_column : str
            Target column
        sorted_features_list : str
            List with order for learning model
        """
        self.queries = queries
        self.target_column = target_column
        self.sorted_features_list = sorted_features_list
        
    def run(self, df, score_function, references_gini, save_all_columns=False):
        """Run test functions(from old framework)

        Calculated gini for set and column

        Parameters
        ----------
        df : pd.DataFrame
            DataFrame for calculating gini
        score_function : Function
            Function of learning model, what have 2 arguments, dataframe and features
        references_gini : dict
            Main gini for compare by sets
        save_all_columns : bool
            Save all model result to df
        """ 
        features_selection_array = []
        gini_table = []
        
        for feature in tqdm(self.sorted_features_list):
            features_selection_array.append(feature)
            
            pred = score_function(df, features_selection_array)
            
            if save_all_columns:
                name_temp = f'uplift_test_{feature}'
            else:
                name_temp = 'uplift_test'
                    
            df[name_temp] = pred
            
            for q in self.queries:
                y_true = df.query(self.queries[q])[self.target_column]
                y_pred = df.query(self.queries[q])[name_temp]
                gini = simple_metrics.SMFunc.gini(y_true,y_pred)

                gini_table.append({'query':q, 'gini':gini, 'feature_append':feature, 
                                                         'features_len':len(features_selection_array), 
                                                         'all_features':json.dumps(features_selection_array), 
                                                         'score_name':name_temp})
                
    
        self.gini_table = pd.DataFrame(gini_table)

        self.set_references_to_table(references_gini)
        
        self.gini_table_color_text = ModelUpliftForward.get_table_color_text(self.queries, self.gini_table, references_gini)
        
    @staticmethod
    def get_table_color_text(queries, gini_table, references_gini):
        """Get color

        Parameters
        ----------
        queries : pd.DataFrame
            Dict with queries
        gini_table : Function
            Dataframe with result calculating model
        references_gini : dict
            Main gini for compare by sets
        """ 
        gini_table_color_text = []
        for q in queries:
            conclusion_color, conclusion_text = ModelUpliftForward.get_result_conclusion(gini_table[gini_table['query'] == q], references_gini[q])
            
            gini_table_color_text.append({'query':q, 'color':conclusion_color, 'text':conclusion_text})
                
        gini_table_color_text = pd.DataFrame(gini_table_color_text)
        
        return gini_table_color_text
    
    @staticmethod
    def plot_uplift_test(df, line_level, column_gini_name='gini', column_count_features='features_len', title = 'Тест 2.4. Uplift на всей выборке', figsize=(12,8), label_uplift = 'uplift на валидационной выборке', label_line = 'Gini финальной модели на валидационной выборке', xlabel='Количество предикторов модели', ylabel='Gini'):
        """Plot uplift result

        Parameters
        ----------
        queries : pd.DataFrame
            Dict with queries
        gini_table : Function
            Dataframe with result calculating model
        references_gini : dict
            Main gini for compare by sets
        """
        plt.figure()
        df.plot(x=column_count_features, y=column_gini_name, grid=True, title=title, figsize=figsize, label=label_uplift)
        plt.axhline(y=line_level,linestyle = '--',lw = 2,color = 'green', label = label_line)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.legend([label_uplift, label_line],loc='best')
        plt.show()
        
    def set_references_to_table(self, references_gini):
        self.references_gini = references_gini
        for q in self.queries:
            print(q, references_gini[q])
            self.gini_table.loc[self.gini_table['query'] == q, 'reference_gini'] = references_gini[q]
        self.gini_table['percent'] = self.gini_table['gini'] * 100 / self.gini_table['reference_gini']
        
    def save_test(self, path = '', name = 'test_4'):
        
        dict_save = {
            'conclusion_color':self.conclusion_color,
            'conclusion_text':self.conclusion_text,
            'queries':self.queries,
            'target_column':self.target_column,
            'gini_table':self.gini_table,
            'sorted_features_list':self.sorted_features_list,
            'gini_table_color_text':self.gini_table_color_text
        }
        
        with open(f'{path}{name}.pickle', 'wb') as handle:
            pickle.dump(dict_save, handle)
            
    def load_test(self, path = '', name = 'test_4'):
        with open(f'{path}{name}.pickle', 'rb') as handle:
            dict_save = pickle.load(handle)
            
        self.conclusion_color = dict_save['conclusion_color']
        self.conclusion_text = dict_save['conclusion_text']
        self.queries = dict_save['queries']
        self.target_column = dict_save['target_column']
        self.gini_table = dict_save['gini_table']
        self.sorted_features_list = dict_save['sorted_features_list']
        self.gini_table_color_text = dict_save['gini_table_color_text']
        
        
    @staticmethod
    def get_result_conclusion(gini_table, reference_gini):        
        if max(gini_table['percent']) >= 101:
            color = 'y'
            text = 'При исключении 1 или нескольких факторов качество модели растет на 1% и более (относительно).'
        elif max(gini_table['percent']) >= 105:
            color = 'r'
            text = 'При исключении 1 или нескольких факторов качество модели растет на 5% и более (относительно).'
        else:
            color = 'g'
            text = 'В модели отсутствуют предикторы, добавление которых в список предикторов модели приводит к существенному снижению gini модели.'

        return color, text
    
    def get_result_text(self):
        return self.conclusion_text

    def get_result_color(self, size=3):
        validation_utils.ValidationUtils.plot_lights(self.conclusion_color, size)
        return self.conclusion_color