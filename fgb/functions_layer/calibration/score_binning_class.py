import pandas as pd
import numpy as np
import optbinning as opt
from sklearn.isotonic import IsotonicRegression

class score_binning():
    def __init__(self 
                 ,clb_set 
                 ,target_name 
                 ,target_prev_name
                 ,score_name
                ):
        '''
        Parameters :
        -------------
        clb_set : pd.Dataframe
            Dataset to calculate calib
            
        target_name : string
            Name of calibration target
            
        target_prev_name : string
            Name of calibration target, with low maturity vintages
            
        score_name : string
            Name of score for calibration
        '''
        self.clb_set = clb_set.copy()
        self.target_name = target_name
        self.target_prev_name = target_prev_name
        self.score_name = score_name
    
    def build_bins_table(self,bins_name):
        gr = self.clb_set.groupby(by=bins_name).agg({self.score_name: ['min','max','mean','count']
                                                     ,self.target_name : ['count','sum','mean']
                                                     ,self.target_prev_name : ['count','sum','mean']
                                                    })
        gr.insert(0, 'bin_size', gr[(self.score_name,'count')]/gr[(self.score_name,'count')].sum())
        return gr.reset_index(drop=False)
    
    def isotonic_bins(self
                      ,target_type = 'target' # target or target_prev                      
                      ,y_min =0.001 # минимальный пребин по DR, всё что ниже в 1 грейд 
                      ,y_max = 1 # максимальный пребин
                      ,bins_save_name = 'isot_bins' 
                      ,increasing = 'auto'
                      ,out_of_bounds = 'clip'                      
                      ,**isot_params):
        '''
        Parameters :
        -------------
            target_type : string
                'target' or 'target_prev'
                Determines target to make isotonic binning
            
            increasing : bool or 'auto', default=True
                Determines whether the predictions should be constrained to increase
                
            y_min : float, default= 0.0005
                Lower bound on the lowest predicted value (the minimum value may
                still be higher). If not set, defaults to -inf.

            y_max : float, default= 0.1
                Upper bound on the highest predicted value (the maximum may still be
                lower). If not set, defaults to +inf.
                
            bins_save_name : 'string', default = 'isot_bins'
                Name of column in calib_set to save isotonic score bins
                
            **isot_params : sklearn.isotonic.IsotonicRegression params 
        '''
        if target_type == 'target':
            target = self.target_name
        elif target_type == 'target_prev':
            target = self.target_prev_name
            
        isot_params['increasing']=increasing    
        isot_params['y_min'] = y_min 
        isot_params['y_max'] = y_max 
        isot_params['out_of_bounds'] = out_of_bounds  
        isot_reg = IsotonicRegression(**isot_params)
        isot_reg.fit(self.clb_set[self.score_name],self.clb_set[target])  
        
        self.clb_set[bins_save_name] = isot_reg.predict(self.clb_set[self.score_name])
    
    def optb_bins(self
                  ,target_type = 'target'
                  ,min_bin_n_event=1
                  ,min_bin_n_nonevent=1
                  ,min_event_rate_diff=0
                  ,bins_save_name = 'optb_bins'
                  ,user_splits_table = pd.DataFrame()
                  ,plot_bins = True
                  ,return_optb_table = True                  
                  ,**optb_params):
        '''
        Parameters :
        -------------
            target_type : string
                'target' or 'target_prev'
                Determines target to make isotonic binning
                
            min_bin_n_event : int or None, optional (default=None)
                The minimum number of event records for each bin. If None,
                ``min_bin_n_event = 1``.
                
            min_bin_n_nonevent : int or None, optional (default=None)
                The minimum number of non-event records for each bin. If None,
                ``min_bin_n_nonevent = 1``.   
                
            min_event_rate_diff : float, optional (default=0)
                The minimum event rate difference between consecutives bins. This
                option currently only applies when ``monotonic_trend`` is "ascending",
                "descending", "peak_heuristic" or "valley_heuristic".
            
            bins_save_name : 'string', default = 'optb_bins'
                Name of column in calib_set to save isotonic score bins
                
            user_splits_table : dp.DataFrame
                Result of build_bins_table
            
            **optb_params : optbinning.OptimalBinning params
        '''
        if target_type == 'target':
            target = self.target_name
        elif target_type == 'target_prev':
            target = self.target_prev_name
        
        optb_params['min_bin_n_event']=min_bin_n_event
        optb_params['min_bin_n_nonevent']=min_bin_n_nonevent
        optb_params['min_event_rate_diff']=min_event_rate_diff
        
        if len(user_splits_table)>0:
            optb_params['user_splits'] = np.sort(user_splits_table[(self.score_name,'min')].values)
        
        optb = opt.OptimalBinning(**optb_params)
        optb.fit(self.clb_set[self.score_name],self.clb_set[target])       
        self.clb_set[bins_save_name] = optb.transform(self.clb_set[self.score_name])
        
        bt = optb.binning_table.build()
        
        if plot_bins :
            optb.binning_table.plot(metric = 'event_rate')
        
        if return_optb_table : 
            return bt, optb
        
        