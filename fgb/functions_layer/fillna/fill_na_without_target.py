import numpy as np
import pandas as pd

class FillNaWithoutTarget():
    """
        Functional class for fill value without target
    """

    @staticmethod
    def fill_na_wo_target(
        feature
        ,thresholds
        ,min_share_to_split : float = 0.05
        ,merge_na_with : str = 'biggest_bin'
        ,default_value = np.nan
    ):
        '''Fill na without target

        Parametrs
        ---------
        feature : nd.array
            values of feature
        thresholds : array
            thresholds of bin
        min_share_to_split : float
            min share of NaNs to split in a separated bin
        merge_na_with : str
            biggest_bin - merge NaNs to biggest bin
            smallest_bin - merge NaNs to smallest bin
            highest_value - highest value of feature + 1
            least_value - least value of feature - 1

        Returns
        -------
        scalar
            return value
        '''

        vc = pd.cut(feature,[-np.inf]+thresholds).value_counts(normalize=True,dropna=False).sort_index()
        NaN_share = pd.isna((feature)).sum()

        fillna_value = default_value

        if (NaN_share > min_share_to_split) or (NaN_share==0):
            return default_value

        if merge_na_with == 'biggest_bin':
            ind = vc[vc.index.notna()].argmax()
        if merge_na_with == 'smallest_bin':
            ind = vc[vc.index.notna()].argmin()
        if merge_na_with == 'highest_value':
            ind = len(vc[vc.index.notna()]) - 1
        if merge_na_with == 'least_value':
            ind = 0

        bin_borders = vc.index[ind]
        fillna_value = bin_borders.mid

        if np.isneginf(fillna_value):
            fillna_value = feature.min()-1

        if np.isposinf(fillna_value):
            fillna_value = feature.max()+1

        return fillna_value
        