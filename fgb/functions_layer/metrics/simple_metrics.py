from fgb.core_layer.metrics import core_gini
import numpy as np
import sklearn.metrics as metrics
import matplotlib.pyplot as plt

class SMFunc():
    """
        Simple class with many static metrics
    """

    @staticmethod
    def gini(y_true, y_pred, rnd=8, sample_weight=None):
        """Calculated gini by core class.

        Have two error handler

        Parameters
        ----------
        y_true : array
            true answer(array with 0 and 1)
        y_pred : array
            predict from model
        rnd : int
            round by python
        sample_weight : array
            weight for samples

        """
        if sum(y_true) == 0:
            return 'Error: y_true not have 1'
        
        if sum(np.isinf(y_true)) > 0:
            return 'Error: y_true have inf'

        return round(core_gini.CoreGini.gini(y_true=y_true, y_pred=y_pred, sample_weight=sample_weight), rnd)

    @staticmethod
    def plot_roc(y_true, y_pred, title, size=(10, 10), color_fill='green', fontsize=16, fontsize_legend=14):
        """Plot roc curve

        Parameters
        ----------
        y_true : array
            true answer(array with 0 and 1)
        y_pred : array
            predict from model
        title : String
            title of plot
        size : tuple
            size plot
        color_fill : String
            color fill

        """
        fig, ax = plt.subplots(figsize=size)

        fpr, tpr, threshold = metrics.roc_curve(y_true, y_pred)
        roc_auc = metrics.auc(fpr, tpr)

        ax.plot(fpr, tpr, 'green', label = 'AUC = %0.2f' % roc_auc)
        ax.set(ylim=(0, 1), xlim=(0, 1))
        ax.fill_between(fpr, fpr, tpr, color='green', lw=3, alpha=0.2)
        ax.plot([0, 1], [0, 1],'k--', lw=3, label='random classifier')
        ax.legend(loc = 'lower right', fontsize=fontsize)
        ax.set_title(title, fontsize=fontsize)
        ax.set_xlabel('False Positive Rate', fontsize=fontsize)
        ax.set_ylabel('True Positive Rate', fontsize=fontsize)
        ax.legend(fontsize=fontsize_legend)
        ax.tick_params(axis='both', which='major', labelsize=fontsize)
        ax.grid()

        return fig, ax