import numpy as np
import pandas as pd

class UniformBinning():
    """
        Functional class for create uniform binning, this binning have a equal value for every bins
    """

    @staticmethod
    def numerical_feature_equisize_binning(feature : np.ndarray
                                            ,bin_size : float = 0.1
                                            ,min_bin_size : float = 0.05
                                            ):
        '''uniform binning with protect of very small binning

        Parametrs
        ---------
        feature : np.ndarray
            values of feature
        bin_size : float
            expected share in bin
        min_bin_size : float
            least share in bin

        Returns
        -------
        dict
            return thresholds
        '''

        na_cnt = np.isnan(feature).sum()
        values, counts = np.unique(feature, return_counts=True)
        cumsum = np.cumsum(counts)

        if bin_size<min_bin_size: # есле перепутаны местами, то правит, иначе, будет баговать
            bin_size, min_bin_size = min_bin_size, bin_size

        expected_bin = int(len(feature) * bin_size)
        min_bin = int(len(feature) * min_bin_size)

        thresholds = []
        not_binned = sum(counts) - na_cnt
        # print(f'start not_binned = {not_binned}')

        while not_binned > min_bin:

            size_diff = np.abs(cumsum - expected_bin)
            ind = size_diff.argmin()

            last_bin_size = cumsum[ind]
            cumsum = cumsum - last_bin_size
            not_binned = not_binned - last_bin_size

            # print(f'\t tres = {values[ind]} not_binned = {not_binned}')
            if (last_bin_size <= min_bin):
                ind = ind + 1

            last_bin_size = cumsum[ind]
            cumsum = cumsum - last_bin_size
            not_binned = not_binned - last_bin_size

            if not_binned <= min_bin:
                # print('break')
                break
            thresholds.append(values[ind])
        
        return thresholds+[np.inf]
