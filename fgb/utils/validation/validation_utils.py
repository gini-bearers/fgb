import matplotlib.pyplot as plt

class ValidationUtils:
    """
        Class utils for validation
    """

    @staticmethod
    def plot_lights(color, size=3, return_flag = False):
        """Plot lightning

        Parameters
        ----------
        color : string
            color - 'g' - green, 'y' - yellow, 'r' - red
        size : int
            size of lightning
        return_flag : bool
            return plot parametrs

        """

        # Initialise the subplot function using number of rows and columns 
        figure, axis = plt.subplots(1, 3, figsize=(size,size/3)) 

        circle_1 = plt.Circle((0.5, 0.5), 0.40, color='red', fill=False)
        circle_2 = plt.Circle((0.5, 0.5), 0.40, color='yellow', fill=False)
        circle_3 = plt.Circle((0.5, 0.5), 0.40, color='green', fill=False)

        if color=='r':
            circle_1 = plt.Circle((0.5, 0.5), 0.40, color='red', fill=True)
        elif color=='y':
            circle_2 = plt.Circle((0.5, 0.5), 0.40, color='yellow', fill=True)
        elif color=='g':
            circle_3 = plt.Circle((0.5, 0.5), 0.40, color='green', fill=True)
        else:
            return 'Error: change color to "r", "y", "g"'


        axis[0].add_patch(circle_1)
        axis[0].axis('off')
        axis[1].add_patch(circle_2)
        axis[1].axis('off')
        axis[2].add_patch(circle_3)
        axis[2].axis('off')


        # Combine all the operations and display 
        plt.show() 

        if return_flag:
            return figure, axis