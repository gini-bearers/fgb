import logging
import os

class LogHelpers():
    # https://habr.com/ru/companies/wunderfund/articles/683880/

    logger = None

    def __init__(self, log_name, directory='log', file_name='test.log', format_msg='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s'):
        log_filename = directory + '/' + file_name
        #print('111')
        os.makedirs(os.path.dirname(log_filename), exist_ok=True)
        #print(log_filename)

        #logging.basicConfig(filename=log_filename,
        #                    filemode='a',
        #                    format=format_msg,
        #                    datefmt=date_format,
        #                    level=logging.INFO)
        # print('222')
        # Create the logger
        # Admin_Client: The name of a logger defined in the config file
        # self.logger = logging.getLogger(log_name)
        # print('333')

        # получение пользовательского логгера и установка уровня логирования
        self.logger = logging.getLogger(log_filename)
        if len(self.logger.handlers) > 0:
            print('Warning:Logger handlers more then 1')

        self.logger.setLevel(logging.DEBUG)

        # настройка обработчика и форматировщика в соответствии с нашими нуждами
        py_handler = logging.FileHandler(log_filename, mode='a')
        py_formatter = logging.Formatter(format_msg)

        # добавление форматировщика к обработчику 
        py_handler.setFormatter(py_formatter)
        # добавление обработчика к логгеру
        self.logger.addHandler(py_handler)

        # py_logger.info(f"Testing the custom logger for module...")

    def write_message(self, msg, level='info'):
        if level == 'info':
            self.logger.info(msg)
        elif level == 'debug':
            self.logger.debug(msg)
        elif level == 'warning':
            self.logger.warning(msg)
        elif level == 'error':
            self.logger.error(msg)
        elif level == 'critical':
            self.logger.critical(msg)
        else:
            self.logger.info(msg)
            
    def close(self):
        logging.shutdown()