import numpy as np
import pandas as pd

class CorePSI():
    """
        Core class for calculating PSI(Population Stability Index)
    """
    
    @staticmethod # расчет PSI
    def psi(
        expected: pd.core.series.Series
        ,actual : pd.core.series.Series
        ,binned : bool = False
        ,thresholds = None
        ,fillna_value = np.nan        
    ):
        '''Calculate the PSI for a single variable


        Parameters
        ----------
        expected : pd.Series 
           expected values
        actual : pd.Series 
           actual values
        thresholds : Array 
           right-included thesholds of bin
        fillna_value : Scalar
           value to fill NaNs

        Returns
        -------
        dict
           Return 3 field:
               psi - summary psi
               psi_table - table of psi
               fillna_value - value to fill NaNs
        '''

        def scale_range (input, min, max):
            input += -(np.min(input))
            input /= np.max(input) / (max - min)
            input += min
            return input
        if binned == False:
            exp = pd.cut(expected.fillna(fillna_value),[-np.inf]+ thresholds)
            act = pd.cut(actual.fillna(fillna_value),[-np.inf]+ thresholds)
        if binned == True:
            exp = expected
            act = actual
            
        exp = exp.value_counts(normalize=True,dropna=False).sort_index()
        exp.name = 'expected'
        
        act = act.value_counts(normalize=True,dropna=False).sort_index()
        act.name = 'actual'
        
        psi_table = pd.merge(exp,act,left_index=True,right_index=True,how='outer')
        psi_table.fillna(0,inplace=True)

        def sub_psi(e_perc, a_perc):
            '''Calculate the actual PSI value from comparing the values.
               Update the actual value to a very small number if equal to zero
            '''
            if a_perc == 0:
                a_perc = 0.0001
            if e_perc == 0:
                e_perc = 0.0001

            value = (e_perc - a_perc) * np.log(e_perc / a_perc)
            return(value)
        psi_table['sub_psi'] = psi_table.apply(lambda x : sub_psi(x['expected'], x['actual']),axis=1)


        return {'psi' : psi_table.sub_psi.sum(), 'psi_table':psi_table, 'fillna_value':fillna_value}