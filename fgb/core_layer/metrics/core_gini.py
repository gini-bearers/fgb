from sklearn.metrics import roc_auc_score

class CoreGini():
    """
        Core class for calculating gini
    """

    @staticmethod
    def gini(y_true, y_pred, sample_weight=None):
        """Calculated row metrics gini.

        Parameters
        ----------
        y_true : array
            true answer(array with 0 and 1)
        y_pred : array
            predict from model
        sample_weight : array
            weight for samples

        """
        return 2*roc_auc_score(y_true, y_pred, sample_weight=sample_weight) - 1
